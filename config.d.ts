import "react-redux";
import { ApplicationState } from "@store/index";
declare module "react-redux" {
	interface DefaultRootState extends ApplicationState {}
}
