export const renderPrice = (price) => {
	if (price < 0.4) return "£";
	if (price < 0.6) return "££";
	if (price < 0.8) return "£££";
	return "££££";
};

export const capitalizeFirstLetter = (string) => {
	return string.charAt(0).toUpperCase() + string.slice(1);
};
