import _ from "lodash";

export const getColourPalette = (scent) => {
	let topCount = 0;
	let middleCount = 0;
	let baseCount = 0;

	let top = {};
	let middle = {};
	let base = {};
	let colorPallete = [];
	Object.keys(scent).forEach(function (index) {
		var children = scent[index]["children"];
		let childTopCount = 0;
		let childMiddleCount = 0;
		let childBaseCount = 0;
		let scentImage;
		Object.keys(children).forEach(function (childIndex) {
			scentImage = children[childIndex]["image"];

			if (children[childIndex]["top"] != 0) {
				topCount = topCount + children[childIndex]["top"];
				childTopCount = childTopCount + children[childIndex]["top"];
			}

			if (children[childIndex]["middle"] != 0) {
				middleCount = middleCount + children[childIndex]["middle"];
				childMiddleCount = childMiddleCount + children[childIndex]["middle"];
			}

			if (children[childIndex]["base"] != 0) {
				baseCount = baseCount + children[childIndex]["base"];
				childBaseCount = childBaseCount + children[childIndex]["base"];
			}
		});

		if (childTopCount > 0) {
			top[index] = {
				name: scent[index].name,
				image: scentImage,
				total: childTopCount,
				color: scent[index].color,
			};
			colorPallete.push(scent[index].color);
		}
		if (childMiddleCount > 0) {
			middle[index] = {
				name: scent[index].name,
				image: scentImage,
				total: childMiddleCount,
				color: scent[index].color,
			};
			colorPallete.push(scent[index].color);
		}
		if (childBaseCount > 0) {
			base[index] = {
				name: scent[index].name,
				image: scentImage,
				total: childBaseCount,
				color: scent[index].color,
			};
			colorPallete.push(scent[index].color);
		}
	});

	let calculatedTopMiddleBase = {
		top: {
			scents: top,
			total: topCount,
		},
		middle: {
			scents: middle,
			total: middleCount,
		},
		base: {
			scents: base,
			total: baseCount,
		},
	};

	let topMiddleBase = calculatedTopMiddleBase;

	/* TO BE REMOVED IN REACT - USE STATE TO LOOP THROUGH COLOR ARRAY ON DIV INSTEAD */

	let colorString = "";
	for (const color of colorPallete) {
		colorString += color + ",";
	}
	colorString = colorString.slice(0, -1);
	return [colorString, topMiddleBase];
};

export const createNewScent = (selectedScents) => {
	let userScent = {};
	let newScent = {};
	let percentage;

	selectedScents.forEach(function (scent, index) {
		/* Convert percentage to decimal */
		percentage = scent.value / 100;
		/* Divide all values in original scent by selected percentage value */
		const percentageOfScent = _.cloneDeepWith(scent, (x) =>
			typeof x === "number" ? Math.round(x * percentage) : undefined
		);
		/* Create custom scent */
		newScent = _.mergeWith(newScent, percentageOfScent, (a, b) => {
			if (_.every([a, b], _.isNumber)) return a + b;
		});
	});

	userScent = newScent["fragrances"];
	return userScent;
};
