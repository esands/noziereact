import React, { useEffect, useRef, useState } from "react";
import Pie from "@Components/charts/Pie";
import Bar from "@Components/charts/Bar";
import Radar from "@Components/charts/Radar";

import ScentName from "@Components/common/ScentName";
import TinySlider from "tiny-slider-react";
import "tiny-slider/dist/tiny-slider.css";

const settings = {
	items: 1,
	center: true,
	loop: false,
	slideBy: 1,
	mouseDrag: true,
	controls: false,
	nav: false,
	gutter: 30,
};

const Data = ({ data, scentName, setScentName }) => {
	const sliderRef = useRef(null);
	const onGoTo = (dir) => {
		sliderRef?.current?.slider.goTo(dir);
		setTitle(getSliderTitle());
	};

	const getSliderTitle = () => {
		const title = ["Bar Chart", "Pie Chart", "Radar Chart"];
		const index = sliderRef?.current?.slider.getInfo().index;
		return title[index];
	};
	const [title, setTitle] = useState(getSliderTitle());

	useEffect(() => {
		if (sliderRef.current) {
			setTitle(getSliderTitle());
		}
	}, [sliderRef]);

	return (
		<div id="main" className="position-relative">
			<div className="container">
				<ScentName
					scentName={scentName}
					setScentName={(ev) => setScentName(ev)}
				/>
				<div id="topMiddleBaseRow">
					<div className="container">
						<div className="row text-center align-items-center justify-content-center">
							<div className="col-4 col-md-2">
								<h6>TOP</h6>
								<div id="topPercentage">{data && data.top.total}%</div>
							</div>
							<div className="col-4 col-md-2">
								<h6>MIDDLE</h6>
								<div id="middlePercentage">{data && data.middle.total}%</div>
							</div>
							<div className="col-4 col-md-2">
								<h6>BASE</h6>
								<div id="basePercentage">{data && data.base.total}%</div>
							</div>
						</div>
					</div>
				</div>
				<div id="chartContainer" className="my-5">
					<div id="mobileChartNav" className="d-block d-md-none">
						<div className="row align-items-center justify-content-center text-center mb-4">
							<div className="col-6">
								<div className="row">
									<div
										className="chartNav col-2"
										onClick={() => onGoTo("prev")}
									>
										&#8249;
									</div>
									<div className="col-8" id="activeChart">
										{title}
									</div>

									<div
										onClick={() => onGoTo("next")}
										className="chartNav col-2"
									>
										&#8250;
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="row d-flex d-md-none">
						<TinySlider settings={settings} ref={sliderRef}>
							<div id="radar" className=" col-md-4 ">
								<Bar data={data} />
							</div>
							<div id="bar" className=" col-md-4 ">
								<Pie data={data} />
							</div>
							<div id="bar" className=" col-md-4 ">
								<Radar data={data} />
							</div>
						</TinySlider>
					</div>
					<div className="row d-none d-md-flex">
						<div id="radar" className=" col-md-4 ">
							<Bar data={data} />
						</div>

						<div id="bar" className=" col-md-4 ">
							<Pie data={data} />
						</div>
						<div id="bar" className=" col-md-4 ">
							<Radar data={data} />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Data;
