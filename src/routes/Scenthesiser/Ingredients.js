import ScentName from "@Components/common/ScentName";
import BottleItImg from "@Img/bottleIt.png";
import { IngredientCard } from "../../components/ingredientsTab";
import { useSelector } from "react-redux";

const Ingredients = ({ data, scentName, setScentName }) => {
	const topMiddleBase = useSelector((state) => state.builtBlend.topMiddleBase);
	const { top, middle, base } = topMiddleBase || {};

	const selectedBlends = useSelector(
		(state) => state.builtBlend.selectedBlends
	);

	return (
		<div className="blend-ingredients">
			<div className="aboveDroplets position-relative">
				<div className="container-md">
					<ScentName
						scentName={scentName}
						setScentName={(ev) => setScentName(ev)}
					/>
					<div className="fade-in">
						<div className="row justify-content-center align-items-center border-bottom">
							<div className="col-10 col-lg-3 text-center">
								<div className="row text-uppercase">
									<div className="col-4 text-center">
										<h4>Top</h4>
									</div>
									<div className="col-4 text-center">
										<h4>Middle</h4>
									</div>
									<div className="col-4 text-center">
										<h4>Base</h4>
									</div>
								</div>
								<div className="row">
									<div className="col-4 text-center">
										<h2>{top.total}%</h2>
									</div>
									<div className="col-4 text-center">
										<h2>{middle.total}%</h2>
									</div>
									<div className="col-4 text-center">
										<h2>{base.total}%</h2>
									</div>
								</div>
							</div>
						</div>
						<div className="row my-3">
							<div className="col-12 text-center">
								<h4 className="text-uppercase">Your Blends</h4>
							</div>
						</div>

						<div className="row">
							{" "}
							{selectedBlends?.map((blend) => {
								return <IngredientCard blend={blend} />;
							})}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Ingredients;
