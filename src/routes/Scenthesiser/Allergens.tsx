import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import BottleIt from "../../components/common/BottleIt";
import { FAQItem } from "../../components/FAQTab";

const SectionHeader = styled.h3`
	font-size: 1.25rem;
	padding-left: 1rem;
`;

const Allergens = () => {
	return (
		<div className="faqs">
			<div className="aboveDroplets position-relative">
				<div className="container-md">
					<div className="row justify-content-center align-items-center mt-3">
						<div className="col-12 text-center">
							<h3>what’s in it?</h3>
							<h2>Here are the allergens you may need to be aware of</h2>
							<BottleIt />
						</div>
					</div>
					<div className="row">
						<FAQItem
							question={"Italian Bergamot"}
							answer={
								"This is the answer This is the answer This is the answer This is the answer This is the answer"
							}
							isAllergen
						/>
						<FAQItem
							question={"Italian Bergamot"}
							answer={
								"This is the answer This is the answer This is the answer This is the answer This is the answer"
							}
							isAllergen
						/>
						<FAQItem
							question={"Italian Bergamot"}
							answer={
								"This is the answer This is the answer This is the answer This is the answer This is the answer"
							}
							isAllergen
						/>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Allergens;
