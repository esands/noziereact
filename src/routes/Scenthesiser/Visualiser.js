import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.min.css";
import "swiper/swiper.min.css";
import { useEffect } from "react";
import BackgroundVisualiser from "@Components/visualiser";
import ScentName from "@Components/common/ScentName";
import Sliders from "@Components/sliders";

import JoyRide from "@Components/modals/JoyRide";

import { useDispatch } from "react-redux";

const Visualiser = ({
	colourPalette,
	showSliders,
	scentName,
	setScentName,
	top,
	middle,
	base,
	handleShowSlider,
	handleSliderChange,
	inputState,
}) => {
	const dispatch = useDispatch();

	return (
		<div
			id="animatedGradient"
			style={{
				backgroundImage: `linear-gradient(-45deg, ${colourPalette})`,
			}}
		>
			<div id="bubbleWrap">
				<div className="bubble x1"></div>
				<div className="bubble x2"></div>
				<div className="bubble x3"></div>
				<div className="bubble x4"></div>
				<div className="bubble x5"></div>
				<div className="bubble x6"></div>
				<div className="bubble x7"></div>
				<div className="bubble x8"></div>
				<div className="bubble x9"></div>
				<div className="bubble x10"></div>
			</div>

			<div className="aboveDroplets position-relative">
				<div className="container">
					<ScentName
						scentName={scentName}
						setScentName={(ev) => setScentName(ev)}
						lightText
					/>
					<BackgroundVisualiser top={top} middle={middle} base={base} />
				</div>

				<Sliders
					showSliders={showSliders}
					handleShowSlider={handleShowSlider}
				/>
			</div>
			<JoyRide />
		</div>
	);
};

export default Visualiser;
