import React, { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";

const LandingPage = () => {
	const location = useLocation();
	const dispatch = useDispatch();

	useEffect(() => {}, [dispatch, location.pathname]);
	return (
		<main className="container-xxl">
			<div className="row mb-2">
				<div className="col-md-12 text-center text-intro py-3">
					<h1>Title sss</h1>
				</div>
			</div>
		</main>
	);
};

export default LandingPage;
