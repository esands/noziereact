import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useFormik } from "formik";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { Typography } from "@material-ui/core";
import StyledSelect from "../components/inputs/StyledSelect";
import Checkbox from "@material-ui/core/Checkbox";
import { withStyles } from "@material-ui/core/styles";
import { formPost } from "../app/axios";
import DateFnsUtils from "@date-io/date-fns";

import {
	MuiPickersUtilsProvider,
	KeyboardDatePicker,
} from "@material-ui/pickers";
import * as yup from "yup";
import { useSelector } from "react-redux";
import { selectActionText } from "../redux/content";

const StyledTextField = withStyles((theme) => ({
	root: {
		backgroundColor: "#fff",
		marginLeft: theme.spacing(1),
		marginRight: theme.spacing(1),
		fontFamily: `"Raleway", sans-serif`,

		"& fieldset": {
			backgroundColor: "#fff",
		},
		"& input": {
			position: "relative",
			zIndex: "1000",
		},

		"& label.Mui-focused": {
			color: "#3B1429",
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "#3B1429",
		},
		"& .MuiOutlinedInput-root": {
			"& fieldset": {
				borderColor: "#3B1429",
			},
			"&:hover fieldset": {
				borderColor: "#3B1429",
			},
			"&.Mui-focused fieldset": {
				borderColor: "#3B1429",
			},
		},
	},
}))(TextField);

const StyledDatePicker = withStyles((theme) => ({
	root: {
		backgroundColor: "#fff",
		marginLeft: theme.spacing(1),
		marginRight: theme.spacing(1),
		"& fieldset": {
			backgroundColor: "#fff",
		},
		"& input": {
			position: "relative",
			zIndex: "1000",
		},
		"& .MuiIconButton-root": {
			position: "relative",
			zIndex: "1000",
		},
		"& .MuiInputAdornment-positionEnd": {
			position: "relative",
			zIndex: "1000",
		},

		fontFamily: `"Raleway", sans-serif`,
		"& label.Mui-focused": {
			color: "#3B1429",
		},
		"& .MuiTypography-colorPrimary": {
			color: "#3B1429",
		},
		"& ..MuiPickersYear-root": {
			color: "#3B1429",
		},

		"& .MuiInput-underline:after": {
			borderBottomColor: "#3B1429",
		},
		"& .MuiOutlinedInput-root": {
			"& fieldset": {
				borderColor: "#3B1429",
			},
			"&:hover fieldset": {
				borderColor: "#3B1429",
			},
			"&.Mui-focused fieldset": {
				borderColor: "#3B1429",
			},
		},
	},
}))(KeyboardDatePicker);

const StyledLabel = withStyles((theme) => ({
	root: {
		width: "100%",
		alignItems: "start",
		"& MuiFormControlLabel-labels": {
			marginLeft: "1rem",
		},
	},
}))(FormControlLabel);

const StyledCheckbox = withStyles((theme) => ({
	root: {
		"& svg": {
			backgroundColor: "rgba(59, 20, 41, 0.04)",
			fill: "#3B1429",
		},
		"&:hover svg": {
			backgroundColor: "rgba(59, 20, 41, 0.04)",
			fill: "#3B1429",
		},
	},
	checked: {
		color: "#3B1429",
		"&:hover": {
			backgroundColor: "rgba(59, 20, 41, 0.04) !important",
			fill: "#3B1429",
		},
	},
	colorSecondary: {
		color: "#3B1429 !important",
		"&:hover": {
			backgroundColor: "rgba(59, 20, 41, 0.04)",
			fill: "#3B1429",
		},
		"&$checked": {
			backgroundColor: "rgba(59, 20, 41, 0.04)",
			fill: "#3B1429",
		},
	},
}))(Checkbox);

const Label = withStyles((theme) => ({
	root: {
		marginLeft: ".5rem",
		fontWeight: "700",
	},
}))(Typography);

const validationSchema = yup.object({
	email: yup
		.string("Enter your email")
		.email("Enter a valid email")
		.required("Email is required"),
});

const Form = () => {
	const [dobError, setDobError] = useState("");
	const text = useSelector(selectActionText);
	const history = useHistory();
	const formik = useFormik({
		initialValues: {
			firstName: "",
			lastName: "",
			email: "",
			interest: "",
			dob: new Date(),
			terms: false,
		},
		validationSchema: validationSchema,
		onSubmit: (values) => {
			formPost("signup", values);
			history.push("recipe");
		},
	});

	const date = new Date();
	date.setFullYear(date.getFullYear() - 18);

	return (
		<div className="container-lg">
			<div className="row">
				<form onSubmit={formik.handleSubmit}>
					{text && text.form && (
						<div className="col-12  col-lg-6 mx-auto text-intro">
							<div className="mb-4 ms-4">
								{" "}
								<h1>{text.form}</h1>
							</div>
						</div>
					)}
					<div className="col-12  col-lg-6 mx-auto d-flex justify-content-center form-group">
						<StyledLabel
							control={
								<StyledTextField
									id="firstName"
									name="firstName"
									value={formik.values.firstName}
									onChange={formik.handleChange}
									fullWidth // eslint-disable-line
									style={{ margin: 8 }}
									placeholder="Type your name here...."
									margin="normal"
									variant="outlined"
									InputLabelProps={{
										shrink: true,
									}}
								/>
							}
							label={<Label>What's your first name?</Label>}
							labelPlacement="top"
						/>
					</div>

					<div className="col-12  col-lg-6 mx-auto d-flex justify-content-center form-group">
						<StyledLabel
							control={
								<StyledTextField
									id="lastName"
									name="lastName"
									value={formik.values.lastName}
									onChange={formik.handleChange}
									style={{ margin: 8 }}
									placeholder="Type your name here...."
									variant="outlined"
									margin="normal"
									fullWidth // eslint-disable-line
									InputLabelProps={{
										shrink: true,
									}}
								/>
							}
							label={<Label>What's your last name?</Label>}
							labelPlacement="top"
						/>
					</div>

					<div className="col-12  col-lg-6 mx-auto d-flex justify-content-center form-group">
						<StyledLabel
							control={
								<StyledTextField
									id="email"
									name="email"
									value={formik.values.email}
									onChange={formik.handleChange}
									error={formik.touched.email && Boolean(formik.errors.email)}
									helperText={formik.touched.email && formik.errors.email}
									fullWidth // eslint-disable-line
									style={{ margin: 8 }}
									placeholder="Type your email here...."
									margin="normal"
									variant="outlined"
									InputLabelProps={{
										shrink: true,
									}}
								/>
							}
							label={<Label>What's your email address?</Label>}
							labelPlacement="top"
						/>
					</div>
					<div className="col-12 col-lg-6 mx-auto d-flex flex-column justify-content-center form-group">
						<label className="d-block  ms-4 mb-2" htmlFor="interest">
							What are you interested in?
						</label>

						<StyledSelect
							id="interest"
							name="interest"
							inputId="interest"
							isSearchable={false}
							value={formik.values.interest}
							onChange={(val) => {
								formik.setFieldValue("interest", val);
							}}
							options={[
								{
									value: "Malt extract kit brewers",
									label: "Malt extract kit brewers",
								},
								{
									value: "Part-extract, part-grain brewers",
									label: "Part-extract, part-grain brewers",
								},

								{ value: "All-grain brewers", label: "All-grain brewers" },
							]}
							className="ms-4 mb-2 me-2"
						/>
					</div>
					<MuiPickersUtilsProvider utils={DateFnsUtils}>
						<div className="col-12  col-lg-6 mx-auto d-flex justify-content-center form-group">
							<StyledLabel
								control={
									<StyledDatePicker
										disableToolbar
										variant="inline"
										format="MM/dd/yyyy"
										margin="normal"
										id="date-picker-inline"
										openTo="year"
										value={formik.values.dob}
										onChange={(val) => {
											if (date < val) {
												setDobError("You must be at least 18 years old.");
											} else {
												setDobError("");
												formik.setFieldValue("dob", val);
											}
										}}
										KeyboardButtonProps={{
											"aria-label": "change date",
										}}
										fullWidth // eslint-disable-line
										style={{ margin: 8 }}
										placeholder="Type your name here...."
										InputLabelProps={{
											shrink: true,
										}}
										inputVariant="outlined"
									/>
								}
								label={<Label>What's your date of birth?</Label>}
								labelPlacement="top"
							/>
						</div>
						{dobError && (
							<div className="col-12  col-lg-6 mx-auto d-flex justify-content-start form-group">
								<p className="error ms-4 MuiFormHelperText-root MuiFormHelperText-contained Mui-error MuiFormHelperText-filled">
									{dobError}
								</p>
							</div>
						)}
					</MuiPickersUtilsProvider>
					<div className="col-12  col-lg-6 mx-auto d-flex justify-content-center form-group mb-4">
						<StyledLabel
							control={
								<StyledCheckbox
									id="terms"
									name="terms"
									checked={formik.values.terms}
									onChange={formik.handleChange}
									required
									disableRipple
								/>
							}
							label={
								<Label>
									<p>Lets stay in touch.</p>{" "}
									<p>
										Please tick this box to confirm that you are happy to be
										contacted for marketing purposes via email. Click here to
										view our{" "}
										<a
											id="privacy-link"
											rel="noreferrer"
											target="_blank"
											href="https://www.muntons.com/privacy-policy/"
										>
											Privacy Policy
										</a>
									</p>
								</Label>
							}
							labelPlacement="start"
							className="ms-4 me-2"
						/>
					</div>
					<div className="col-12  col-lg-6 mx-auto d-flex justify-content-center form-group mb-4">
						<button type="submit" className="btn btn-secondary w-100">
							Create my recipe and register
						</button>
					</div>

					<div className="col-12  col-lg-6 mx-auto d-flex justify-content-center form-group mb-5">
						<p
							onClick={() => history.push("recipe")}
							className="text-center cursor-pointer text-decoration-underline w-100"
						>
							Skip this
						</p>
					</div>
				</form>
			</div>
		</div>
	);
};

export default Form;
