import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { useLocation, Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Pie from "@Components/charts/Pie";
import { setBlendData } from "@Redux/builtBlend";
import { selectScents } from "@Redux/scent";
import { getColourPalette, createNewScent } from "../helpers/scenthesiser";

import { Visualiser, Data, Ingredients, Allergens } from "./Scenthesiser";
import FAQ from "./FAQ";

const Overview = () => {
	const location = useLocation();
	const dispatch = useDispatch();
	const [scentName, setMixtureName] = useState("");
	const [inputState, setInputState] = useState([40, 60]);
	const [showSliders, setShowSliders] = useState(false);
	const scents = useSelector((state) => state.blends.blends);
	const selectedBlends = useSelector(
		(state) => state.builtBlend.selectedBlends
	);
	const [userScent, setUserScent] = useState(createNewScent(selectedBlends));
	const [initialColourPalette, initialTopMiddleBase] =
		getColourPalette(userScent);
	const [colourPalette, setColourPalette] = useState(initialColourPalette);
	const [topMiddleBase, setTopMiddleBase] = useState(initialTopMiddleBase);

	const handleShowSlider = () => {
		setShowSliders(!showSliders);
	};

	const handleSliderChange = (e, i) => {
		const newInputState = [...inputState];
		newInputState[i] = parseInt(e.target.value);
		setInputState(newInputState);
	};

	const setScentName = (val) => {
		localStorage.setItem( 'scentName', val );
		setMixtureName(val);
	};
	useEffect(() => {
		const newUserScent = createNewScent(selectedBlends);
		setUserScent(newUserScent);
		const [newColourPalette, newTopMiddleBase] = getColourPalette(userScent);
		setTopMiddleBase(newTopMiddleBase);
		setColourPalette(newColourPalette);
		dispatch(setBlendData([newTopMiddleBase, newColourPalette]));
		setMixtureName(localStorage.getItem( 'scentName' ) || "");
	}, [inputState, showSliders]);
	let { top, middle, base } = topMiddleBase;
	return (
		<>
			<Switch>
				<Route path="/scenthesiser/data">
					{topMiddleBase && (
						<Data
							data={topMiddleBase}
							scentName={scentName}
							setScentName={(val) => setScentName(val)}
						/>
					)}
				</Route>
				<Route path="/scenthesiser/ingredients">
					{topMiddleBase && (
						<Ingredients
							data={topMiddleBase}
							scentName={scentName}
							setScentName={(val) => setScentName(val)}
						/>
					)}
				</Route>
				<Route path="/scenthesiser/faq">
					<FAQ />
				</Route>
				<Route path="/scenthesiser/allergens">
					<Allergens />
				</Route>
				<Route path="/scenthesiser">
					<Visualiser
						colourPalette={colourPalette}
						showSliders={showSliders}
						scentName={scentName}
						setScentName={(val) => setScentName(val)}
						top={top}
						middle={middle}
						base={base}
						handleShowSlider={(ev, val) => handleShowSlider(ev, val)}
						handleSliderChange={(ev, val) => handleSliderChange(ev, val)}
						inputState={inputState}
					/>
				</Route>
			</Switch>
		</>
	);
};

export default Overview;
