import React, { useEffect } from "react";
import { useLocation, Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { resetBlend } from "@Redux/builtBlend";
import { fetchBlendsAsync, clearAllFilters } from "@Redux/blends";
import BergamontImg from "../img/bergamot_2.png";
import OrangeImg from "../img/orange_2-1.png";
import LemonImg from "../img/lemon_2.png";
import JasmineImg from "../img/jasmine.png";
import NozieLogo from "../img/nozieLogo2.png";

const LandingPage = () => {
	const location = useLocation();
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(clearAllFilters());
		dispatch(resetBlend());
		dispatch(fetchBlendsAsync());
	}, [dispatch, location.pathname]);
	return (
		<div className="welcome">
			<div id="animatedGradient">
				<div className="aboveDroplets position-relative vertical-center">
					<div className="container">
						<div className="row justify-content-center align-items-center fade-in">
							<div className="col-10 col-md-5 text-center">
								<img src={NozieLogo} alt="" />
								<h3 className="text-black">welcome!</h3>
								<h2>To the Nozie Scenethesiser</h2>
								<h4>
									The Scenthesiser is a bespoke perfume configuration tool
									designed to help you create scents you love your way.
								</h4>
								<Link to="/qualification">
									Click anywhere to start building your perfume{" "}
								</Link>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="row background-fruit">
				<div className="col-12">
					<div className="row h-50 justify-content-between">
						<div className="col-2 col-md-5 align-self-end">
							<div className="text-center">
								<img src={BergamontImg} alt="" className="lime" />
							</div>
						</div>
						<div className="col-6 col-md-6 align-self-center">
							<div className="text-center mt-5">
								<img src={JasmineImg} alt="" />
							</div>
						</div>
					</div>
					<div className="row h-50 justify-content-between">
						<div className="col-6 col-md-6 align-self-end align-self-md-center">
							<div className="text-center">
								<img src={OrangeImg} alt="" />
							</div>
						</div>
						<div className="col-2 col-md-2 align-self-end ">
							<div className="text-end hide-overflow">
								<img src={LemonImg} alt="" className="w-100 remove-white-bg" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default LandingPage;
