import BottleImg from "../img/bottle.png";

const BottleSize = () => {
	return (
		<div class="choose-bottle-size">
			<div class="aboveDroplets position-relative">
				<div class="container-md">
					<div class="row justify-content-center align-items-center mt-3">
						<div class="col-12 text-center">
							<h3>Choose your bottle size</h3>
							<h2>Select the bottle size you would like</h2>
						</div>
					</div>
					<div class="row mt-4 fade-in">
						<div class="col-12 col-lg-5 mb-3 mb-md-0">
							<p class="mb-md-4 fw-500">Popular</p>
							<div class="row h-100 bottle-size selected">
								<div class="col-6 col-md-8">
									<img src={BottleImg} alt="" class="w-100" />
								</div>
								<div class="col-6 col-md-4">
									<label class="form-check-label" for="check1">
										<div class="p-2">
											<div class="row">
												<div class="col-12">
													<div class="form-check float-end">
														<label class="form-check-label" for="check1">
															Select
														</label>
														<input
															class="form-check-input"
															type="radio"
															id="check1"
															name="option1"
															value="something"
														/>
													</div>
												</div>
											</div>
											<div class="col-12">
												<p class="mb-2 fw-500">50ML Bottle</p>
											</div>
											<div class="col-12">
												<p class="mb-2 fw-500">£36</p>
											</div>
											<div class="col-12 reward-stars fw-500">
												<i class="fas fa-star"></i> 250{" "}
												<span>reward stars</span>
											</div>
											<div class="col-12 mt-3">
												<div class="dropdown box-shadow p-2">
													<a
														class="dropdown-toggle fw-400"
														type="button"
														id="dropdownMenuButton"
														data-toggle="dropdown"
														aria-haspopup="true"
														aria-expanded="false"
													>
														Qty <span id="quantity50ml">1</span>
													</a>
													<div
														class="dropdown-menu"
														aria-labelledby="dropdownMenuButton"
													>
														<a
															class="dropdown-item"
															href="#"
															onclick="changeQuantity(1, 'quantity50ml')"
														>
															1
														</a>
														<a
															class="dropdown-item"
															href="#"
															onclick="changeQuantity(2, 'quantity50ml')"
														>
															2
														</a>
														<a
															class="dropdown-item"
															href="#"
															onclick="changeQuantity(3, 'quantity50ml')"
														>
															3
														</a>
													</div>
												</div>
											</div>
										</div>
									</label>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg">
							<p class="mb-md-4 fw-500">More size options</p>
							<div class="row mb-3 mb-lg-2 bottle-size">
								<div class="col-6">
									<img src={BottleImg} alt="" class="w-100" />
								</div>
								<div class="col-6">
									<label class="form-check-label" for="check2">
										<div class="p-2">
											<div class="row">
												<div class="col-12">
													<div class="form-check float-end">
														<label class="form-check-label" for="check2">
															Select
														</label>
														<input
															class="form-check-input"
															type="radio"
															id="check2"
															name="option1"
															value="something"
														/>
													</div>
												</div>
											</div>
											<div class="col-12">
												<p class="mb-2 fw-500">100ML Bottle</p>
											</div>
											<div class="col-12">
												<p class="mb-2 fw-500">£50</p>
											</div>
											<div class="col-12 reward-stars fw-500">
												<i class="fas fa-star"></i> 600{" "}
												<span>reward stars</span>
											</div>
											<div class="col-12 mt-3">
												<div class="dropdown box-shadow p-2">
													<a
														class="dropdown-toggle fw-400"
														type="button"
														id="dropdownMenuButton"
														data-toggle="dropdown"
														aria-haspopup="true"
														aria-expanded="false"
													>
														Qty <span id="quantity100ml">1</span>
													</a>
													<div
														class="dropdown-menu"
														aria-labelledby="dropdownMenuButton"
													>
														<a
															class="dropdown-item"
															href="#"
															onclick="changeQuantity(1, 'quantity100ml')"
														>
															1
														</a>
														<a
															class="dropdown-item"
															href="#"
															onclick="changeQuantity(2, 'quantity100ml')"
														>
															2
														</a>
														<a
															class="dropdown-item"
															href="#"
															onclick="changeQuantity(3, 'quantity100ml')"
														>
															3
														</a>
													</div>
												</div>
											</div>
										</div>
									</label>
								</div>
							</div>
							<div class="row mt-3 bottle-size">
								<div class="col-6">
									<img src={BottleImg} alt="" class="w-100" />
								</div>
								<div class="col-6">
									<label class="form-check-label" for="check3">
										<div class="p-2">
											<div class="row">
												<div class="col-12">
													<div class="form-check float-end">
														<label class="form-check-label" for="check3">
															Select
														</label>
														<input
															class="form-check-input"
															type="radio"
															id="check3"
															name="option1"
															value="something"
														/>
													</div>
												</div>
											</div>
											<div class="col-12">
												<p class="mb-2 fw-500">10ML Bottle</p>
											</div>
											<div class="col-12">
												<p class="mb-2 fw-500">£10</p>
											</div>
											<div class="col-12 reward-stars fw-500">
												<i class="fas fa-star"></i> 50 <span>reward stars</span>
											</div>
											<div class="col-12 mt-3">
												<div class="dropdown box-shadow p-2">
													<a
														class="dropdown-toggle fw-400"
														type="button"
														id="dropdownMenuButton"
														data-toggle="dropdown"
														aria-haspopup="true"
														aria-expanded="false"
													>
														Qty <span id="quantity10ml">1</span>
													</a>
													<div
														class="dropdown-menu"
														aria-labelledby="dropdownMenuButton"
													>
														<a
															class="dropdown-item"
															href="#"
															onclick="changeQuantity(1, 'quantity10ml')"
														>
															1
														</a>
														<a
															class="dropdown-item"
															href="#"
															onclick="changeQuantity(2, 'quantity10ml')"
														>
															2
														</a>
														<a
															class="dropdown-item"
															href="#"
															onclick="changeQuantity(3, 'quantity10ml')"
														>
															3
														</a>
													</div>
												</div>
											</div>
										</div>
									</label>
								</div>
							</div>
						</div>
						<div class="col-12 col-lg my-3 mt-md-5">
							<div class="row bottle-size">
								<div class="col-6">
									<img src={BottleImg} alt="" class="w-100" />
								</div>
								<div class="col-6">
									<label class="form-check-label" for="check4">
										<div class="p-2">
											<div class="row">
												<div class="col-12">
													<div class="form-check float-end">
														<label class="form-check-label" for="check4">
															Select
														</label>
														<input
															class="form-check-input"
															type="radio"
															id="check4"
															name="option1"
															value="something"
														/>
													</div>
												</div>
											</div>
											<div class="col-12">
												<p class="mb-2 fw-500">30ML Bottle</p>
											</div>
											<div class="col-12">
												<p class="mb-2 fw-500">£24</p>
											</div>
											<div class="col-12 reward-stars fw-500">
												<i class="fas fa-star"></i> 150{" "}
												<span>reward stars</span>
											</div>
											<div class="col-12 mt-3">
												<div class="dropdown box-shadow p-2">
													<a
														class="dropdown-toggle fw-400"
														type="button"
														id="dropdownMenuButton"
														data-toggle="dropdown"
														aria-haspopup="true"
														aria-expanded="false"
													>
														Qty <span id="quantity30ml">1</span>
													</a>
													<div
														class="dropdown-menu"
														aria-labelledby="dropdownMenuButton"
													>
														<a
															class="dropdown-item"
															href="#"
															onclick="changeQuantity(1, 'quantity30ml')"
														>
															1
														</a>
														<a
															class="dropdown-item"
															href="#"
															onclick="changeQuantity(2, 'quantity30ml')"
														>
															2
														</a>
														<a
															class="dropdown-item"
															href="#"
															onclick="changeQuantity(3, 'quantity30ml')"
														>
															3
														</a>
													</div>
												</div>
											</div>
										</div>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<footer class="sticky-footer sticky-footer-blend">
					<div class="row justify-content-center align-items-center h-100">
						<div class="col-md-4 col-12 text-center">
							<button class="text-uppercase apply">
								CONFIRM & CHOOSE BOTTLE
							</button>
						</div>
					</div>
				</footer>
			</div>
		</div>
	);
};

export default BottleSize;
