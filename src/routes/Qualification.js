import React, { useEffect } from "react";
import { useLocation, Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import WomenImg from "@Img/example_women1.png";
import WomenImg2 from "@Img/example_women2.png";
import WomenImg3 from "@Img/example_women3.png";
import {
	setIsInspiredBy,
	setIsMoodWallFilterOpen,
	setIsCategoryFilterOpen,
} from "../redux/blends";

const LandingPage = () => {
	const location = useLocation();
	const dispatch = useDispatch();
	const handleInspiredByClick = () => {
		dispatch(setIsInspiredBy(true));
	};

	const handleMoodClick = () => {
		dispatch(setIsMoodWallFilterOpen(true));
	};

	const handleCategoriesClick = () => {
		dispatch(setIsCategoryFilterOpen(true));
	};
	useEffect(() => {}, [dispatch, location.pathname]);
	return (
		<div className="blends">
			<div className="aboveDroplets position-relative">
				<div className="container-md">
					<div className="row justify-content-center align-items-center mt-3">
						<div className="col-12">
							<div className="text-center">
								<h3>lets get started</h3>
								<h2>Choose your foundation blend</h2>
							</div>
						</div>
					</div>
					<div className="row justify-content-between mt-3">
						<div className="col-12 col-md mx-md-3">
							<div className="blend-option blend-option-1">
								<div className="row gx-0 ">
									<div className="col-md-12 col-6 blend-option-img w-md-100">
										<img src={WomenImg} alt="" />
									</div>
									<div className="col-md-12 col-6 blend-option-bottom">
										<div className="py-5 ps-md-5 ps-4 blend-option-orange h-100">
											<h3>option 1</h3>
											<h2>From our ‘inspired by’ fragrance range</h2>
											<Link onClick={handleInspiredByClick} to="/blends">
												Start &gt;
											</Link>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div className="col-12 col-md mx-md-3">
							<div className="blend-option blend-option-2">
								<div className="row gx-0">
									<div className="col-md-12 col-6 blend-option-img w-md-100 order-2 order-md-1">
										<img src={WomenImg2} alt="" />
									</div>
									<div className="col-md-12 col-6 blend-option-bottom order-1 order-md-2">
										<div className="py-5 ps-md-5 ps-4 h-100">
											<h3>option 2</h3>
											<h2>Based on a mood, occasion or style</h2>
											<Link onClick={handleMoodClick} to="/blends">
												Start &gt;
											</Link>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div className="col-12 col-md mx-md-3">
							<div className="blend-option blend-option-3">
								<div className="row gx-0">
									<div className="col-md-12 col-6 blend-option-img w-md-100">
										<img src={WomenImg3} alt="" />
									</div>
									<div className="col-md-12 col-6 blend-option-bottom">
										<div className="py-5 ps-md-5 ps-4 h-100">
											<h3>option 3</h3>
											<h2>From our popular fragrance categories</h2>
											<Link onClick={handleCategoriesClick} to="/blends">
												Start &gt;
											</Link>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default LandingPage;
