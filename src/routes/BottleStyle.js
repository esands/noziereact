import BottleWhite from "../img/bottle_white.png";
import BottleBlack from "../img/bottle_black.png";
import { useState } from "react";
const BottleStyle = () => {
	const [color, setColor] = useState("white");
	const [capColor, setCapColor] = useState("white");
	const [isCap, updateIsCap] = useState(false);
	return (
		<>
			<div class="choose-perfume">
				<div class="aboveDroplets position-relative">
					<div class="container-md">
						<div class="row justify-content-center align-items-center mt-3">
							<div class="col-12 text-center">
								<h3>now choose your bottlee</h3>
								<h2>Tap on the bottle you would like</h2>
							</div>
						</div>
						<div class="row justify-content-center mt-3">
							<div class="col-6 text-end">
								<img src={BottleBlack} alt="" class="bottle-colour" />
							</div>

							<div class="col-6">
								<img src={BottleWhite} alt="" class="bottle-colour" />
							</div>
						</div>
					</div>
					<footer class="sticky-footer sticky-footer-blend">
						<div class="row justify-content-center align-items-center h-100">
							<div class="col-md-4 col-12 text-center">
								<button class="text-uppercase apply">
									CONFIRM & CHOOSE BOTTLE
								</button>
							</div>
						</div>
					</footer>
				</div>
			</div>
			<div class="choose-cap">
				<div class="aboveDroplets position-relative">
					<div class="container-md">
						<div class="row justify-content-center align-items-center mt-3">
							<div class="col-12 text-center">
								<h3>now choose your bottle</h3>
								<h2>Tap on the bottle you would like</h2>
							</div>
						</div>
						<div class="row d-block d-lg-none text-center">
							<div class="col">
								<h3 class="mb-3">the name of your perfume</h3>
								<h1>Luke’s Aventus</h1>
								<h2>Edit</h2>
							</div>
						</div>
						<div class="row justify-content-center justify-content-lg-end mt-5">
							<div class="col-12 col-lg-4 text-center  order-2 order-lg-1">
								<div class="row d-none d-lg-block">
									<div class="col">
										<h3 class="mb-3">the name of your perfume</h3>
										<h1>Luke’s Aventus</h1>
										<h2>Edit</h2>
									</div>
								</div>

								<div class="row mt-5">
									<div class="col">
										<div class="row tabs">
											<div class={`col ${isCap ? "" : "active" }`} onClick={() => updateIsCap(false)}>
												<h3 class="">choose your bottle colour</h3>
											</div>
											<div class={`col ${isCap ? "active" : "" }`} onClick={() => updateIsCap(true)}>
												<h3 class="">choose your cap colour</h3>
											</div>
										</div>
										<div class="horizontal-scrollable">
											<div class="row mt-3 cap-cols">
												<div class="col-3 col-lg-6 my-3">
													<div class="box white">
														<div
															class=" bottle-bg bottle-bg-mini"
															onClick={() => {
																if (isCap) {
																	setCapColor("white");
																} else {
																	setColor("white");
																}
															}}
														></div>
													</div>
												</div>
												<div class="col-3 col-lg-6 my-3">
													<div class="box brown">
														<div
															class=" bottle-bg bottle-bg-mini"
															onClick={() => {
																if (isCap) {
																	setCapColor("brown");
																} else {
																	setColor("brown");
																}
															}}
														></div>
													</div>
												</div>
												<div class="col-3 col-lg-6 my-3">
													<div class="box red">
														<div
															class=" bottle-bg bottle-bg-mini"
															onClick={() => {
																if (isCap) {
																	setCapColor("red");
																} else {
																	setColor("red");
																}
															}}
														></div>
													</div>
												</div>
												<div class="col-3 col-lg-6 my-3">
													<div class="box orange">
														<div
															class=" bottle-bg bottle-bg-mini"
															onClick={() => {
																if (isCap) {
																	setCapColor("orange");
																} else {
																	setColor("orange");
																}
															}}
														></div>
													</div>
												</div>
												<div class="col-3 col-lg-6 my-3">
													<div class="box light-blue">
														<div
															class=" bottle-bg bottle-bg-mini"
															onClick={() => {
																if (isCap) {
																	setCapColor("light-blue");
																} else {
																	setColor("light-blue");
																}
															}}
														></div>
													</div>
												</div>
												<div class="col-3 col-lg-6 my-3">
													<div class="box blue">
														<div
															class=" bottle-bg bottle-bg-mini"
															onClick={() => {
																if (isCap) {
																	setCapColor("blue");
																} else {
																	setColor("blue");
																}
															}}
														></div>
													</div>
												</div>
												<div class="col-3 col-lg-6 my-3">
													<div class="box green">
														<div
															class=" bottle-bg bottle-bg-mini"
															onClick={() => {
																if (isCap) {
																	setCapColor("green");
																} else {
																	setColor("green");
																}
															}}
														></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-9 col-lg-6 mt-3 order-1 order-lg-2">
								<div class="box" id="display-bottle">
									<div class={`bottle-cap ${capColor}`}></div>
									<div class={`bottle ${color}`}></div>
									<div class="bottle-bg"></div>
									<div class="perfume-name text-center">
										<h2>Luke’s Aventus</h2>
									</div>
								</div>
							</div>
						</div>
					</div>
					<footer class="sticky-footer">
						<div class="row justify-content-center align-items-center h-100">
							<div class="col-6 text-end">
								<button class="text-uppercase clear">Add to basket</button>
							</div>
							<div class="col-6 text-start">
								<button class="text-uppercase apply">Checkout</button>
							</div>
						</div>
					</footer>
				</div>
			</div>
		</>
	);
};

export default BottleStyle;
