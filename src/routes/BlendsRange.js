import React, { useEffect, useState } from "react";
import { useLocation, Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
	Categories,
	Search,
	MoodWallFilter,
	AppliedFilters,
} from "@Components/blendFilters";
import {
	addSelectedBlend,
	selectNextValueToAdd,
	setNextBlendValueToAdd,
	setSelectedBlendsValues,
	selectSelectedBlends,
} from "@Redux/builtBlend";
import {
	selectisCategoryFilterOpen,
	selectisMoodWallFilterOpen,
	selectisSearchFilterOpen,
	selectBlends,
	selectAppliedFilters,
	selectFilteredBySearchTerm,
	clearAllFilters,

} from "@Redux/blends";
import WestOutlinedIcon from "@mui/icons-material/WestOutlined";
import EastOutlinedIcon from "@mui/icons-material/EastOutlined";
import {
	selectBlendsByCategoryFilter,
	selectBlendsByMoodFilter,
} from "../redux/blends";
import BlendsList from "../components/blendsRange/BlendsList";
import {
	CarouselProvider,
	Slider,
	Slide,
	ButtonBack,
	ButtonNext,
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";

const BlendsRange = () => {
	const location = useLocation();
	const dispatch = useDispatch();
	const isInspiredBy = useSelector((state) => state.blends.isInspiredBy);
	const isCategoryFilterShowing = useSelector(selectisCategoryFilterOpen);
	const isMoodWallFilterShowing = useSelector(selectisMoodWallFilterOpen);
	const isSearchFilterOpen = useSelector(selectisSearchFilterOpen);
	const blends = useSelector(selectBlends);
	const appliedFilters = useSelector(selectAppliedFilters);
	const blendsFilteredBySearchTerm = useSelector(selectFilteredBySearchTerm);
	const blendsFilteredByMood = useSelector(selectBlendsByMoodFilter);
	const blendsFilteredByCategory = useSelector(selectBlendsByCategoryFilter);
	const filterType = useSelector((state) => state.blends.filterType);
	const filterMap = {
		mood: blendsFilteredByMood,
		category: blendsFilteredByCategory,
		search: blendsFilteredBySearchTerm,
		all: blends,
	};
	const [selectedBlends, setSelectedBlends] = useState([]);
	const updateMultiButton = (selectedIds) => {
		let blendList = [];
		selectedIds.forEach(id => {
			blendList.push(blends.filter(blend => blend.id == id)[0]);
		})
		setSelectedBlends(blendList);
	}
	useEffect(() => {}, [dispatch, location.pathname]);
	const carousels = [1, 2, 3, 4, 5];
	const preSelectedBlends = useSelector(selectSelectedBlends);
	const baseVals = [100, 80, 60, 52, 52, 50, 52, 51, 52, 55];
	const nextValueToAdd = useSelector(selectNextValueToAdd);

	const sliderCount = useSelector(
		(state) => state.builtBlend.selectedBlends.length
	);
	const selectedFoundationBlend = useSelector(
		(state) => state.builtBlend.selectedBlends[0]
	);
	return (
		<>
			{isCategoryFilterShowing && <Categories />}
			{isSearchFilterOpen && <Search />}
			{isMoodWallFilterShowing && <MoodWallFilter />}
			{!isCategoryFilterShowing &&
				!isMoodWallFilterShowing &&
				!isSearchFilterOpen && (
					<div className="blend-ranges">
						<div id="animatedGradient">
							<div className="aboveDroplets position-relative">
								<div className="container-md">
									<div className="row justify-content-center align-items-center mt-3">
										<div className="col-12">
											<div className="text-center">
												<h3 className="text-black">Choose a blend</h3>
											</div>
										</div>
										<AppliedFilters />
									</div>
									{!isInspiredBy ? (
										<div className="carousel slide row">
											<div className="carousel-inner">
												<div className="carousel-item active">
													<BlendsList updateMultiButton={updateMultiButton} 
													blends={filterMap[filterType] || []} />
												</div>
											</div>
										</div>
									) : (
										<CarouselProvider
											naturalSlideWidth={100}
											naturalSlideHeight={125}
											totalSlides={5}
										>
											<Slider>
												{carousels.map((carousel) => {
													return (
														<Slide index={carousel} key={carousel}>
															<div
																className="carousel slide"
																data-bs-ride="carousel"
																data-bs-interval="false"
															>
																<div className="carousel-inner">
																	<div className="carousel-item active">
																		<div className="row">
																			<div className="col-12 text-center">
																				<h2 className="slider-title">
																					<ButtonBack
																						style={{
																							border: "none",
																							background: "none",
																						}}
																					>
																						{" "}
																						<WestOutlinedIcon className="mx-5" />
																					</ButtonBack>
																					Inspired By {carousel}
																					<ButtonNext
																						style={{
																							border: "none",
																							background: "none",
																						}}
																					>
																						<EastOutlinedIcon className="mx-5" />
																					</ButtonNext>
																				</h2>
																			</div>
																		</div>
																		<BlendsList
																			blends={filterMap[filterType] || []}
																			updateMultiButton={updateMultiButton}
																		/>
																	</div>
																</div>
															</div>
														</Slide>
													);
												})}{" "}
											</Slider>
										</CarouselProvider>
									)}
								</div>
								<div
								className={`sticky-footer sticky-footer-blend ${selectedBlends.length > 0 ? "" : "d-none"}`}>
									<div className="row justify-content-center align-items-center h-100">
										<div className="col-md-4 col-10 text-center">
											<Link to="/scenthesiser/visualiser">
												<button
													className="text-uppercase apply"
													onClick={() => {
														let newNextValue = nextValueToAdd;

														const valueMap = {
															0: 100,
															1: 20,
															2: 20,
														};											
														
														//Loop through all the new items set in the multi-select
														selectedBlends.forEach((blend,index) => {
															let addValue = 0;
															//If there are no blends that have been selected previously
															if (preSelectedBlends.length == 0) {
																//If we're at the first item in the array, set the base blend value
																if (index == 0) {
																	//Set the value to be added, then subtract that from 100, the remaining amount
																	//will be divided up between the non-base blends.
																	addValue = baseVals[selectedBlends.length - 1];
																	newNextValue = 100 - addValue;
																} else {
																	//If we're not at the first item, assign the value based on the divided up remains of the total.
																	addValue = newNextValue/(selectedBlends.length - 1);
																}
															} else {
																//If the total amount of blends, both already selected and newly selected are less than 3 (for easy division)
																if (preSelectedBlends.length + selectedBlends.length <= 3) {
																	//Figure out what the base blend value currently is, based on how many already selected blends there are.
																	const foundationBlend = sliderCount == 1 ? 100 : 80;
																	//The below values need to be set for the dispatch, but have minimal effect on the blend values in this instance.
																	dispatch(setNextBlendValueToAdd(valueMap[sliderCount]));
																	const totalToDistribute = Math.floor(valueMap[sliderCount]);
																	const totalToAddToFoundation = foundationBlend % sliderCount;
																	//Set the value to be added on the blend we're currently on
																	addValue = valueMap[sliderCount];
																	//Update the base blend (foundationBlend) basedon how many new blends are being added. 
																	dispatch(
																		setSelectedBlendsValues({
																			totalToDistribute,
																			totalToAddToFoundation,
																			foundationBlend: foundationBlend - (valueMap[sliderCount]*selectedBlends.length),
																		})
																	);
																} else {
																	//Hard set the base blend to be 50, minor adjustments will be made to account rounding.
																	const foundationBlend = 50;
																	dispatch(
																		setNextBlendValueToAdd(Math.floor(foundationBlend / (sliderCount + selectedBlends.length)))
																	);

																	//The below calculations take into account 1 new extra blend, if more than 1 is being added, it will need to be resolved here.
																	//If there is, assign the new blends length (minus 1 as that is taken into account in sliderCount)
																	//If not, just set it as 0 so it has no effect anywhere.
																	let newBlendsResolver = selectedBlends.length > 1 ? selectedBlends.length - 1 : 0
																	
																	//Divide up the remaining total between the non-base blends, taking into account the already selected blends too.
																	const totalToDistribute = Math.floor(foundationBlend / (sliderCount + newBlendsResolver));
																	//Figure out if there's any remainders from rounding, then add those to the foundation, so the rest of the blends are equal.
																	const totalToAddToFoundation = foundationBlend % (sliderCount + newBlendsResolver);
																	//Assign the new value ready to be dispatched at the end.
																	addValue = Math.floor(foundationBlend / (sliderCount + newBlendsResolver));
													
																	dispatch(
																		setSelectedBlendsValues({
																			totalToDistribute,
																			totalToAddToFoundation,
																			foundationBlend,
																	}));
																}
															}

															dispatch(
																addSelectedBlend({
																	...blend,
																	value: addValue,
																})
															);
														})
														
														dispatch(clearAllFilters());
													}}
												>
													ADD SELECTED BLENDS TO PERFUME
												</button>
											</Link>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				)}
		</>
	);
};

export default BlendsRange;
