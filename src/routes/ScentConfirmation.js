import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import {
	selectSelectedBlends,
} from "@Redux/builtBlend";

const ScentConfirmation = () => {
	const preSelectedBlends = useSelector(selectSelectedBlends);
	return (
		<div className="choose-perfume">
			<div className="aboveDroplets position-relative">
				<div className="container-md">
					<div className="row justify-content-center align-items-center mt-3">
						<div className="col-12 text-center">
							<h3>to finish your perfume</h3>
							<h2>
								Now confirm your perfume and customise your bottle.
							</h2>
						</div>
					</div>
					<div className="row">
						<div className="col-12">
							<h3 className="text-center my-5">{localStorage.getItem( 'scentName' ) || "My Perfume"}</h3>
							{preSelectedBlends.map((blend, i) => (
								<div className="border-bottom single-blend">
									<div className="row  align-items-center">
										<div className="col-5">
											<h2 className="fw-400">
												{blend.scentName + (i == 0 ? " (Foundation)" : ` (Accent ${i})`)}
											</h2>
										</div>
										<div className="col-md-1 col-1">
										<h2>{"£"+(Math.round(blend.value*blend.pricePerUnit*100)/100)}</h2>
									</div>
										<div className="col">
											<h2 className="text-end blend-percentage">{blend.value+"%"}</h2>
										</div>
									</div>
								</div>
							))}
						</div>
					</div>
				</div>
				<footer className="sticky-footer sticky-footer-blend">
					<div className="row justify-content-center align-items-center h-100">
					<div className="col-md-4 col-6 text-center">
							<button className="text-uppercase apply">
								<Link to="/scenthesiser/visualiser" className="text-white">
									Cancel
								</Link>
							</button>
						</div>
						<div className="col-md-4 col-6 text-center">
							<button className="text-uppercase apply">
								<Link to="/bottle/style" className="text-white">
									Confirm and choose bottle style
								</Link>
							</button>
						</div>
					</div>
				</footer>
			</div>
		</div>
	);
};

export default ScentConfirmation;
