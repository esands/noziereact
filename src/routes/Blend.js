import React, { useEffect, useState } from "react";
import { useLocation, Link, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
	addSelectedBlend,
	selectNextValueToAdd,
	setNextBlendValueToAdd,
	setSelectedBlendsValues,
} from "@Redux/builtBlend";
import { selectIngredientsList, clearAllFilters } from "@Redux/blends";
import WestOutlinedIcon from "@mui/icons-material/WestOutlined";
import EastOutlinedIcon from "@mui/icons-material/EastOutlined";
import Herbs from "@Img/herbs.png";
import Woman from "@Img/example_women3.png";
import BlendImg from "@Img/blendImage.png";
import { capitalizeFirstLetter, renderPrice } from "../helpers";

const Blend = () => {
	const location = useLocation();
	let { blendId } = useParams();
	const dispatch = useDispatch();
	const ingredients = useSelector(selectIngredientsList(blendId));

	const selectedBlend = useSelector((state) =>
		state.blends.blends.find((blend) => blend.id == blendId)
	);
	const nextValueToAdd = useSelector(selectNextValueToAdd);

	const randomNumber = Math.floor(Math.random() * 10);
	const blend = {
		name: `Blend Name ${randomNumber}`,
		id: randomNumber,
		value: nextValueToAdd,
	};
	const sliderCount = useSelector(
		(state) => state.builtBlend.selectedBlends.length
	);
	const selectedFoundationBlend = useSelector(
		(state) => state.builtBlend.selectedBlends[0]
	);
	useEffect(() => {
		const valueMap = {
			0: 100,
			1: 20,
			2: 20,
		};

		if (sliderCount == 0) {
			dispatch(setNextBlendValueToAdd(100));
		} else if (sliderCount <= 2) {
			const foundationBlend = sliderCount == 1 ? 100 : 80;
			dispatch(setNextBlendValueToAdd(valueMap[sliderCount]));
			const totalToDistribute = Math.floor(valueMap[sliderCount]);
			const totalToAddToFoundation = foundationBlend % sliderCount;
			dispatch(
				setSelectedBlendsValues({
					totalToDistribute,
					totalToAddToFoundation,
					foundationBlend: foundationBlend - valueMap[sliderCount],
				})
			);
		} else {
			const foundationBlend = 50;
			dispatch(
				setNextBlendValueToAdd(Math.floor(foundationBlend / sliderCount))
			);

			const totalToDistribute = Math.floor(foundationBlend / sliderCount);
			const totalToAddToFoundation = foundationBlend % sliderCount;

			dispatch(
				setSelectedBlendsValues({
					totalToDistribute,
					totalToAddToFoundation,
					foundationBlend,
				})
			);
		}
	}, []);

	return (
		<div className="blend-template">
			<div className="aboveDroplets position-relative">
				<div className="container-md">
					<div className="row justify-content-center my-3 border-bottom">
						<div className="col-12 col-md-6">
							<img src={BlendImg} className="w-100 h-100" alt="" />
						</div>

						<div className="col-12 col-md-6">
							<div className="blend-info">
								<div className="blend-header border-bottom">
									<div className="row">
										<div className="col-9">
											<h2>{selectedBlend.scentName}</h2>
											<p className="mb-3">{selectedBlend.description}</p>
										</div>
										<div className="col-3">
											<p className="blend-price">
												{renderPrice(selectedBlend.pricePerUnit)}
											</p>
										</div>
									</div>
								</div>
								<div className="row">
									<div className="col-12">
										<p>{selectedBlend.longDescription}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-md-4 col-12">
							<img src={Herbs} alt="" className="w-100 mb-4" />
							<img
								src={Woman}
								alt=""
								className="w-100 my-2 d-none d-md-inline-block"
							/>
						</div>
						<div className="col-md-4 col-12 ingredients">
							<h4>Ingredients & Composition</h4>
							<p className="blend-notes fw-900">Top Notes</p>

							{ingredients?.top?.slice(0, 3).map((ingredient, i) => (
								<Ingredient
									key={i}
									name={ingredient[0]}
									value={ingredient[1]}
								/>
							))}
							<p className="blend-notes fw-900">Middle Notes</p>
							{ingredients?.middle?.slice(0, 3).map((ingredient, i) => (
								<Ingredient
									key={i}
									name={ingredient[0]}
									value={ingredient[1]}
								/>
							))}

							<p className="blend-notes fw-900">Base Notes</p>
							{ingredients?.base?.slice(0, 3).map((ingredient, i) => (
								<Ingredient
									key={i}
									name={ingredient[0]}
									value={ingredient[1]}
								/>
							))}
						</div>
						<div className="col-md-4 col-12 d-md-none d-inline-block">
							<img
								src="./img/example_women3.png"
								alt=""
								className="w-100 my-2 "
							/>
						</div>
						<div className="col-md-4 col-12 senses-tags">
							<div className="row">
								<div className="col-12">
									<h4>SENSORY CATEGORIES</h4>
									<div className="sensory-catagories border-bottom">
										{selectedBlend?.moods?.map((mood, i) => (
											<p key={i}>{capitalizeFirstLetter(mood)}</p>
										))}
									</div>
								</div>
							</div>
							<div className="row">
								<div className="col p-0">
									<div className="tags">
										<h4>TAGS</h4>
										{selectedBlend?.categories?.map((category, i) => (
											<div key={i} className="tag">
												{capitalizeFirstLetter(category)}
											</div>
										))}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<footer className="sticky-footer sticky-footer-blend">
					<div className="row justify-content-center align-items-center h-100">
						<div className="col-md-4 col-6 text-center">
							<Link to="/scenthesiser/visualiser">
								<button
									className="text-uppercase apply"
									onClick={() => {
										dispatch(
											addSelectedBlend({
												...selectedBlend,
												value: nextValueToAdd,
											})
										);
										dispatch(clearAllFilters());
									}}
								>
									ADD THIS BLEND TO MY PERFUME AND VIEW MY PERFUME
								</button>
							</Link>
						</div>
						<div className="col-md-4 col-6 text-center">
							<Link to="/blends">
								<button
									className="text-uppercase apply"
									onClick={() => {
										dispatch(
											addSelectedBlend({
												...selectedBlend,
												value: nextValueToAdd,
											})
										);
										dispatch(clearAllFilters());
									}}
								>
									ADD THIS BLEND TO MY PERFUME AND KEEP BROWSING
								</button>
							</Link>
						</div>
					</div>
				</footer>
			</div>
		</div>
	);
};

export default Blend;

const Ingredient = ({ name, value }) => (
	<div className="row">
		<div className="col-10">
			<p>{name}</p>
		</div>
		<div className="col-1">
			<p className="blend-notes m-0">{parseInt(value * 100).toFixed(0)}%</p>
		</div>
	</div>
);
