import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import { Header } from "./components/header/Header";
import {
	LandingPage,
	Qualification,
	Welcome,
	BlendsRange,
	Blend,
	Overview,
	BottleStyle,
	BottleSize,
	ScentConfirmation,
} from "./routes";
import Modal from "react-modal";
import { ModalContent } from "@Components/modals";
import { useDispatch, useSelector } from "react-redux";
import { setIsModalOpen } from "redux/app";

const customStyles = {
	content: {
		top: "50%",
		left: "50%",
		right: "auto",
		bottom: "auto",
		marginRight: "-50%",
		transform: "translate(-50%, -50%)",
		zIndex: "99999999999",
	},
	overlay: {
		zIndex: "9999",
		background: "rgba(0,0,0, 0.6)",
	},
};

// Make sure to bind modal to your appElement (https://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement("#root");

function App() {
	const isOpen = useSelector((state) => state.app.isModalOpen);
	const modalType = useSelector((state) => state.app.modalType);
	const dispatch = useDispatch();
	const onCloseModal = () => {
		dispatch(setIsModalOpen(false));
	};

	return (
		<>
			<Router>
				<Header />

				<Switch>
					<Route path="/qualification">
						<Qualification />
					</Route>
					<Route path="/inspired">
						<LandingPage />
					</Route>
					<Route path="/blend/:blendId">
						<Blend />
					</Route>
					<Route path="/blends">
						<BlendsRange />
					</Route>

					<Route path="/scenthesiser">
						<Overview />
					</Route>
					<Route path="/bottle/confirm">
						<ScentConfirmation />
					</Route>
					<Route path="/bottle/size">
						<BottleSize />
					</Route>
					<Route path="/bottle/style">
						<BottleStyle />
					</Route>
					<Route path="/">
						<Welcome />
					</Route>
				</Switch>
				<Modal
					isOpen={isOpen}
					onRequestClose={onCloseModal}
					style={customStyles}
					contentLabel="Example Modal"
				>
					<ModalContent type={modalType} />
				</Modal>
			</Router>
		</>
	);
}

export default App;
