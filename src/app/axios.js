import axios from "axios";

export const baseUmbracoUrl =
	window.location.hostname === "homebrew.muntons.com"
		? "https://api-homebrew.muntons.com"
		: "https://muntons-api.chickdigital.co.uk";

const baseUrl = "/api/contents/";

const contactBaseUrl = "/api/contact/";

const recipeBaseUrl = "/api/instructions";

const pdfBaseUrk = "/api/pdf";

export const request = (url) =>
	axios.get(baseUmbracoUrl + baseUrl + url).then((res) => res.data);

export const formPost = async (url, body) => {
	axios({
		method: "POST",
		url: baseUmbracoUrl + contactBaseUrl + url,
		data: body,
	}).then((res) => res.data);
};

export const recipePost = async (body) =>
	axios({
		method: "POST",
		url: baseUmbracoUrl + recipeBaseUrl,
		data: body,
	}).then((res) => res.data);

export const pdfPost = async (body) =>
	axios({
		method: "POST",
		url: baseUmbracoUrl + pdfBaseUrk,
		responseType: "blob",
		data: body,
	}).then((res) => res.data);

export const pdfPostShare = async (body) =>
	axios({
		method: "POST",
		url: baseUmbracoUrl + pdfBaseUrk + "/share",

		data: body,
	}).then((res) => res.data);
