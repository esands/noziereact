import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "./app/store";
import App from "./App";

test("renders Landing page react link", () => {
	const { getByText } = render(
		<Provider store={store}>
			<App />
		</Provider>
	);

	expect(
		getByText(
			/Design your very own beer recipe, or create a brew from our range of suggestions/i
		)
	).toBeInTheDocument();

	expect(getByText("Recipe Designer").closest("a")).toHaveAttribute(
		"href",
		"/recipe-designer/style"
	);

	expect(getByText("Recipe Suggestions").closest("a")).toHaveAttribute(
		"href",
		"/recipe-suggestions"
	);
});
