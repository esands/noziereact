import SmoothImage from "components/common/SmoothImage";
import React from "react";

const index = ({ top, middle, base }) => {
	return (
		<div id="topMiddleBaseVisualiser">
			{top && (
				<div className="top">
					<div className="visualiserTitle">
						<div className="align-items-center row">
							<div className="col-3 col-md-1">TOP</div>
							<div className="col-6 col-md-10">
								<hr />
							</div>
							<div id="topPercentage" className="col-3 col-md-1 text-end">
								{top.total}%
							</div>
						</div>
					</div>
					<div
						id="topBlendContainer"
						className="text-center justify-content-around align-items-center row"
					>
						{top &&
							top.scents &&
							Object.values(top.scents).map((scent, i) => (
								<div className="col-6 col-md-4" key={i}>
									{scent.total < 5 ? (
										<div className="scentTitleBubble">
											{scent.total && scent.total.toFixed(0)}% {scent.name}
										</div>
									) : (
										// <img
										// 	style={{ width: `${scent.total}vw` }}
										// 	src={scent.image}
										// />
										<SmoothImage src={scent.image} width={scent.total} />
									)}
								</div>
							))}
					</div>
				</div>
			)}
			{middle && (
				<div className="middle">
					<div className="visualiserTitle">
						<div className="align-items-center row">
							<div className="col-3 col-md-1">MIDDLE</div>
							<div className="col-6 col-md-10">
								<hr />
							</div>
							<div id="middlePercentage" className="col-3 col-md-1 text-end">
								{middle.total}%
							</div>
						</div>
					</div>
					<div
						id="middleBlendContainer"
						className="text-center justify-content-around align-items-center row"
					>
						{middle &&
							middle.scents &&
							Object.values(middle.scents).map((scent, i) => (
								<div className="col-6 col-md-4" key={i}>
									{scent.total < 5 ? (
										<div className="scentTitleBubble">
											{scent.total && scent.total.toFixed(0)}% {scent.name}
										</div>
									) : (
										<SmoothImage src={scent.image} width={scent.total} />
									)}
								</div>
							))}
					</div>
				</div>
			)}
			{base && (
				<div className="base">
					<div className="visualiserTitle">
						<div className="align-items-center row">
							<div className="col-3 col-md-1">BASE</div>
							<div className="col-6 col-md-10">
								<hr />
							</div>
							<div id="basePercentage" className="col-3 col-md-1 text-end">
								{base.total}%
							</div>
						</div>
					</div>
					<div
						id="baseBlendContainer"
						className="text-center justify-content-around align-items-center row"
					>
						{base &&
							base.scents &&
							Object.values(base.scents).map((scent, i) => (
								<div className="col-6 col-md-4" key={i}>
									{scent.total < 5 ? (
										<div className="scentTitleBubble">
											{scent.total && scent.total.toFixed(0)}% {scent.name}
										</div>
									) : (
										<SmoothImage src={scent.image} width={scent.total} />
									)}
								</div>
							))}
					</div>
				</div>
			)}
		</div>
	);
};

export default index;
