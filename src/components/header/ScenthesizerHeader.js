import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import {
	selectSelectedBlends,
} from "@Redux/builtBlend";

import ShareOutlinedIcon from "@mui/icons-material/ShareOutlined";
import OpacityOutlinedIcon from "@mui/icons-material/OpacityOutlined";
import PieChartOutlineOutlinedIcon from "@mui/icons-material/PieChartOutlineOutlined";
import ColorizeOutlinedIcon from "@mui/icons-material/ColorizeOutlined";
import HelpOutlineOutlinedIcon from "@mui/icons-material/HelpOutlineOutlined";
import ErrorOutlineOutlinedIcon from "@mui/icons-material/ErrorOutlineOutlined";

const ScenthesizerHeader = () => {
	const preSelectedBlends = useSelector(selectSelectedBlends);
	const resolvePrice = () => {
		let total = 0;
		let bottleSize = 100;
		preSelectedBlends.forEach(blend => {
			let volume = (blend.value/100)*bottleSize;
			total += volume*blend.pricePerUnit
			total = Math.round(total * 100) / 100
		});

		return "£"+total
	}
	return (
		<div id="secondNav" className="bg-dark ">
			<div className="container">
				<div className="row justify-content-center align-items-center">
					<div className="col-2 col-md-3">
						<div className="d-inline-block">
							<a className="px-2 px-md-4" href="">
								<ShareOutlinedIcon />
							</a>
							{/* <a className="px-2 px-md-4" href="">
								<FavoriteBorderOutlinedIcon />
							</a> */}
						</div>
					</div>
					<div className="col text-center">
						<div className="d-inline-block" id="joyrideNav">
							<NavLink
								activeClassName="is-active"
								className="px-2 px-md-4 active"
								to="/scenthesiser/visualiser"
							>
								<OpacityOutlinedIcon />
							</NavLink>
							<NavLink
								activeClassName="is-active"
								className="px-2 px-md-4 active"
								to="/scenthesiser/data"
							>
								<PieChartOutlineOutlinedIcon />
							</NavLink>
							<NavLink
								activeClassName="is-active"
								className="px-2 px-md-4 active"
								to="/scenthesiser/ingredients"
							>
								<ColorizeOutlinedIcon />
							</NavLink>
							<NavLink
								activeClassName="is-active"
								className="px-2 px-md-4"
								to="/scenthesiser/faq"
							>
								<HelpOutlineOutlinedIcon />
							</NavLink>
							<NavLink
								activeClassName="is-active"
								className="px-2 px-md-4"
								to="/scenthesiser/allergens"
							>
								<ErrorOutlineOutlinedIcon />
							</NavLink>
						</div>
					</div>
					<div className="col-3 text-end">
						<div className="d-inline">
							<div className="price pb-1" key={preSelectedBlends}>
								<span id="totalPrice">{resolvePrice()}</span> <span>100ml</span>
							</div>
						</div>
					</div>
					<div></div>
				</div>
			</div>
		</div>
	);
};

export default ScenthesizerHeader;
