import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
	selectisCategoryFilterOpen,
	selectisMoodWallFilterOpen,
	selectisSearchFilterOpen,
	setIsCategoryFilterOpen,
	setIsMoodWallFilterOpen,
	setIsSearchFilterOpen,
	setAllFiltersClosed,
} from "@Redux/blends";
import CalendarViewMonthOutlinedIcon from "@mui/icons-material/CalendarViewMonthOutlined";
import FilterAltOutlinedIcon from "@mui/icons-material/FilterAltOutlined";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";

const BlendsFilterHeader = () => {
	const openFilter = (fn, filter) => {
		dispatch(setAllFiltersClosed());
		dispatch(fn(filter));
	};
	const dispatch = useDispatch();
	const isCategoryFilterShowing = useSelector(selectisCategoryFilterOpen);
	const isMoodWallFilterShowing = useSelector(selectisMoodWallFilterOpen);
	const isSearchFilterOpen = useSelector(selectisSearchFilterOpen);
	return (
		<div id="secondNav" className="bg-dark ">
			<div className="container">
				<div className="row justify-content-center align-items-center my-1">
					<div className="col-12 col-lg-7 text-center">
						<div className="d-inline-block">
							<a
								className="px-2 px-md-4 text-uppercase fw-500"
								onClick={() => {
									openFilter(setIsMoodWallFilterOpen, !isMoodWallFilterShowing);
								}}
							>
								<CalendarViewMonthOutlinedIcon />
								Mood Wall
							</a>
							<a
								className="px-2 px-md-4 text-uppercase fw-500 d-inline"
								onClick={() => {
									openFilter(setIsCategoryFilterOpen, !isCategoryFilterShowing);
								}}
							>
								<FilterAltOutlinedIcon />
								Catagories
							</a>
							<a
								className="px-2 px-md-4 text-uppercase fw-500"
								onClick={() => {
									openFilter(setIsSearchFilterOpen, !isSearchFilterOpen);
								}}
							>
								<SearchOutlinedIcon /> Search
							</a>
						</div>
					</div>
					<div></div>
				</div>
			</div>
		</div>
	);
};

export default BlendsFilterHeader;
