import React from "react";

const BottleHeader = () => {
	return (
		<div id="secondNav" className="bg-dark bottle-selection-nav pt-0">
			<div className="container">
				<div className="row justify-content-center align-items-center">
					<div className="col text-center">
						<div className="d-inline-block text-uppercase">
							<a
								className="p-md-3 m-md-0 py-3 mx-2  px-md-4 fw-500 active"
								href="/bottle/confirm"
							>
								Confirm Blends &gt;
							</a>
							<a className="p-md-3 m-md-0 py-3 mx-2  px-md-4 fw-500" href="/bottle/style">
								Customise Bottle &gt;
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default BottleHeader;
