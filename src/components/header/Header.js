import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Link, useLocation, useHistory } from "react-router-dom";
import useGetCurrentPage from "../common/useGetCurrentPage";
import logo from "../../img/nozieLogo.png";
import profile from "../../img/profile.png";
import basket from "../../img/basket.png";
import BlendsFilterHeader from "./BlendsFilterHeader";
import ScenthesizerHeader from "./ScenthesizerHeader";
import BottleHeader from "./BottleHeader";

export const Header = () => {
	const [isBlendPage, isOverviewPage, isBottlePage] = useGetCurrentPage();

	return (
		<header>
			<div className="container">
				<div className="row justify-content-center align-items-center">
					<div className="col">
						<div className="slider-nav">
							<button
								className="navbar-toggler d-md-none"
								type="button"
								data-bs-toggle="offcanvas"
								data-bs-target="#offcanvasNavbar"
								aria-controls="offcanvasNavbar"
							>
								<i className="fas fa-bars" id="hamburger-menu"></i>
								<i className="fas fa-times d-none" id="close-nav"></i>
							</button>
							<div
								className="offcanvas offcanvas-start"
								tabIndex="-1"
								id="offcanvasNavbar"
								aria-labelledby="offcanvasNavbarLabel"
							>
								<div className="offcanvas-body">
									<p>
										<a href="">Launch template &gt;</a>
									</p>
									<p>
										<a href="">Qualification template &gt;</a>
									</p>
									<p>
										<a href="">Blends range mood wall template &gt;</a>
									</p>
									<p>
										<a href="">Blends range catagory filter template &gt;</a>
									</p>
									<p>
										<a href="">Blends range search template &gt;</a>
									</p>
									<p>
										<a href="">Blend template &gt;</a>
									</p>
									<p>
										<a href="">Scenthesiser get started template &gt;</a>
									</p>
									<p>
										<a href="">Scenthesiser overview template &gt;</a>
									</p>
									<p>
										<a href="">Scenthesiser add blend template &gt;</a>
									</p>
									<p>
										<a href="">Scenthesiser data view template &gt;</a>
									</p>
									<p>
										<a href="">Scenthesiser ingredients template &gt;</a>
									</p>
									<p>
										<a href="">Scenthesiser FAQs template &gt;</a>
									</p>
									<p>
										<a href="">Scenthesiser Allergens template &gt;</a>
									</p>
									<p>
										<a href="">Scenthesiser confirm perfume template &gt;</a>
									</p>
									<p>
										<a href="">Scenthesiser bottle size template &gt;</a>
									</p>
									<p>
										<a href="">Scenthesiser bottle template &gt;</a>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div className="col text-center py-2">
						<img src={logo} width="150" />
					</div>
					<div className="col text-end">
						<div className="d-inline">
							<a href="">
								<img className="me-3" src={profile} width="20" />
							</a>
							<a href="">
								<img src={basket} width="20" />
							</a>
						</div>
					</div>
				</div>
			</div>

			{isBlendPage && <BlendsFilterHeader />}
			{isOverviewPage && <ScenthesizerHeader />}
			{isBottlePage && <BottleHeader />}
		</header>
	);
};
