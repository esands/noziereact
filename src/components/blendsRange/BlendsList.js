import { renderPrice } from "../../helpers";
import { useState } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import {
	selectSelectedBlends,
} from "@Redux/builtBlend";

import BlendImage from "@Img/blendImage.png";

const BlendsList = ({ blends, updateMultiButton }) => {
	const [selectedBlends, setSelectedBlends] = useState([]);
	const updateSelectedBlends = (id) => {
		let selected = [...selectedBlends];
		if (selected.includes(id)) {
			let index = selected.indexOf(id);
			selected.splice(index, 1);
		} else {
			selected.push(id);
		}

		setSelectedBlends(selected);
		updateMultiButton(selected);
	};

	return (
		<div className="row mt-3 justify-content-start">
			{blends?.map((blend, i) => {
				return <Blend 
				blend={blend} 
				updateSelectedBlends={updateSelectedBlends} 
				selectedBlends={selectedBlends}
				key={i} />;
			})}
		</div>
			
	);
};

export default BlendsList;

const Blend = ({ blend: { scentName, id, pricePerUnit, description }, updateSelectedBlends, selectedBlends }) => {
	const onClickHandler = (id) => {
		updateSelectedBlends(id);
	};
	const preSelectedBlends = useSelector(selectSelectedBlends);
	return (
		<div className="col-12 col-lg-3 p-md-0">
			<div className="example-blend m-auto mx-lg-3 mb-3">
				<img src={BlendImage} className="w-100" alt="" />

				<div className="row p-3">
					<div className="col-6">
						<h2 className="blend-no">{scentName}</h2>
					</div>
					<div className="col-6">
						<h2 className="blend-price">{renderPrice(pricePerUnit)}</h2>
					</div>
					<div className="col-12">
						<p>{description}</p>
					</div>
					<div className="col-12">
						<Link to={`/blend/${id}`}>Discover this blend &gt;</Link>
					</div>
					<div className="col-12 mt-3 d-flex">
						<p className="ml-auto mb-0 me-2">
							{selectedBlends.includes(id) 
								? (selectedBlends.indexOf(id) + preSelectedBlends.length) === 0 
									? "Base Blend" 
									: "Accent " + (selectedBlends.indexOf(id) + preSelectedBlends.length)
								: "Add this blend to my perfume"}
							</p>
						<input
							className="form-check-input-custom mt-0"
							type="checkbox"
							id="check1"
							name="option1"
							disabled={(selectedBlends.length + preSelectedBlends.length) == 10 && !selectedBlends.includes(id) }
							checked={selectedBlends.includes(id)}
							onChange={() => onClickHandler(id)}
						/>
					</div>
				</div>
			</div>
		</div>
	);
};
