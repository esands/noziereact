const useActionTextFormat = (obj, replacementString) => {
	const formattedText = {};
	for (let [key, value] of Object.entries(obj, replacementString)) {
		formattedText[key] = value.replace("{{styleOfDrink}}", replacementString);
	}
	return formattedText;
};

export default useActionTextFormat;
