import React, { useState } from "react";
import { Link } from "react-router-dom";
import WomanImg from "@Img/example_women4.png";

const props = {
	name: "Test name",
	description: "This is the scent description",
	price: "$$$$",
	data: {
		top: [
			{ name: "note 1", value: "24" },
			{ name: "note 2", value: "14" },
		],
		middle: [
			{ name: "note 3", value: "24" },
			{ name: "note 4", value: "243" },
		],
		base: [
			{ name: "note 5", value: "21" },
			{ name: "note6", value: "22" },
		],
	},
	scentDescription: ["nice", "flowers", "oak"],
};

const IngredientCard = ({ blend }) => {
	const { top, middle, base } = props.data;
	const [isIngredientsOpen, toggleOpenIngredients] = useState(false);
	const [isScentDescriptionOpen, toggleOpenDescription] = useState(false);
	const renderBlendNotes = ({ name, value }, i) => (
		<div className="row" key={`blend-${i}`}>
			<div className="col-9">
				<p>{blend.scentName}</p>
			</div>
			<div className="col-3">
				<p>{blend.value}%</p>
			</div>
		</div>
	);

	return (
		<div className="col-12 col-lg-3 col-sm-6">
			<div className="box-shadow">
				<div className="color-foreground">
					<img src={WomanImg} className="w-100" height="220px" alt="" />
				</div>
				<div className="p-3">
					<div className="row">
						<div className="col-9">
							<h2>{blend.scentName}</h2>
							<p className="mb-3">DESRIPTION</p>
						</div>
						<div className="col-3">
							<h2 className="blend-price">$$$</h2>
						</div>
					</div>

					<div className="row">
						<div className="col-12">
							<div className="border-top">
								<div className="row align-items-center">
									<div className="col-12 text-left">
										<a
											className="catagory-collapse"
											data-bs-toggle="collapse"
											href="#multiCollapseExample1"
											role="button"
											aria-expanded="false"
											aria-controls="multiCollapseExample1"
											onClick={() => toggleOpenIngredients(!isIngredientsOpen)}
										>
											<i
												className={`fas ${
													isIngredientsOpen && "down"
												} arrow float-end fa-sort-up`}
											></i>
											<h6 className="text-uppercase d-inline">Ingredients</h6>
										</a>
									</div>
								</div>

								<div className="row">
									<div className="col">
										<div
											className={`collapse multi-collapse ${
												isIngredientsOpen ? "show" : ""
											}`}
											id="multiCollapseExample1"
										>
											<div className="card card-body">
												<div className="row">
													<div className="col-12 ingredients">
														<p className="blend-notes">Top Notes</p>
														{top && top.map(renderBlendNotes)}

														<p className="blend-notes">Middle Notes</p>
														{middle && middle.map(renderBlendNotes)}

														<p className="blend-notes">Base Notes</p>
														{base && base.map(renderBlendNotes)}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div className="row">
						<div className="col-12">
							<div className="border-top">
								<div className="row align-items-center">
									<div className="col-12 text-left">
										<a
											className="catagory-collapse"
											data-bs-toggle="collapse"
											href="#multiCollapseExample2"
											role="button"
											aria-expanded="false"
											aria-controls="multiCollapseExample2"
											onClick={() =>
												toggleOpenDescription(!isScentDescriptionOpen)
											}
										>
											<i
												className={`fas ${
													isScentDescriptionOpen && "down"
												} arrow float-end fa-sort-up`}
											></i>
											<h6 className="text-uppercase d-inline">
												Scent Description
											</h6>
										</a>
									</div>
								</div>

								<div className="row">
									<div className="col">
										<div
											className={`collapse multi-collapse ${
												isScentDescriptionOpen ? "show" : ""
											}`}
											id="multiCollapseExample2"
										>
											<div className="card card-body">
												<div className="row">
													<div className="col-12">
														<div className="sensory-catagories">
															{props.scentDescription?.map((scent) => (
																<p>
																	<Link to="/">{scent}</Link>
																</p>
															))}
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default IngredientCard;
