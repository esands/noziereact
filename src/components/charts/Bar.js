import React, { useEffect, useRef, useState } from "react";
import "chart.js/auto";
import { Chart } from "react-chartjs-2";

const PieChart = ({ data }) => {
	const canvasRef = useRef(null);
	const [pieData, setPieData] = useState({ labels: [], datasets: [] });
	useEffect(() => {
		const gradientBarCtx = canvasRef.current.getContext("2d");
		let topGradient = gradientBarCtx.createLinearGradient(0, 0, 300, 400);
		let middleGradient = gradientBarCtx.createLinearGradient(0, 0, 300, 400);
		let baseGradient = gradientBarCtx.createLinearGradient(0, 0, 300, 400);
		Object.keys(data).forEach(function(index) {
			if (index == "top") {
				let topScents = data[index].scents;
				let topCount = 0;
				Object.keys(topScents).forEach(function(index) {
					topCount++;

					topGradient.addColorStop(
						topCount / Object.keys(topScents).length,
						topScents[index].color
					);
				});
			} else if (index == "middle") {
				let middleScents = data[index].scents;
				let middleCount = 0;
				Object.keys(middleScents).forEach(function(index) {
					middleCount++;
					middleGradient.addColorStop(
						middleCount / Object.keys(middleScents).length,
						middleScents[index].color
					);
				});
			} else if (index == "base") {
				let baseScents = data[index].scents;
				let baseCount = 0;
				let decimal;
				Object.keys(baseScents).forEach(function(index) {
					baseCount++;
					decimal = "0." + (baseCount + 3);
					baseGradient.addColorStop(
						parseFloat(decimal),
						baseScents[index].color
					);
				});
			}
		});

		const newPieData = {
			labels: ["Top", "Middle", "Base"],
			datasets: [
				{
					data: [data.top.total, data.middle.total, data.base.total],
					backgroundColor: [topGradient, middleGradient, baseGradient],
				},
			],
		};
		setPieData(newPieData);
	}, [data]);

	const options = {
		indexAxis: "y",
		elements: {
			bar: {
				borderWidth: 0,
			},
		},
		responsive: true,
		plugins: {
			legend: {
				display: false,
			},
			title: {
				display: false,
			},
		},
		scales: {
			x: {
				grid: {
					display: false,
					drawBorder: false,
					drawTicks: false,
				},
				ticks: {
					callback: function(val, index) {
						return "";
					},
				},
			},
			y: {
				grid: {
					display: false,
					drawBorder: false,
					drawTicks: false,
				},
			},
		},
	};

	return (
		<>
			<canvas ref={canvasRef} className="d-none" />
			<Chart type="bar" data={pieData} options={options} />
		</>
	);
};

export default PieChart;
