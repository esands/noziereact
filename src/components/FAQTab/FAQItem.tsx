import { ReactJSXElement } from "@emotion/react/types/jsx-namespace";
import React, { useState } from "react";
import FmdBadOutlinedIcon from "@mui/icons-material/FmdBadOutlined";
import styled from "styled-components";

const FAQButtonContainer = styled.div`
	display: flex;
	align-items: flex-end;
	gap: 2rem;
`;

interface FAQItemProps {
	question: string;
	answer: string;
	isAllergen?: boolean;
}

const FAQItem = ({
	question,
	answer,
	isAllergen,
}: FAQItemProps): JSX.Element => {
	const [isOpen, setIsOpen] = useState(false);

	const toggleOpen = () => {
		setIsOpen(!isOpen);
	};

	return (
		<div className="col-12 ">
			<div className="col catagory border-top">
				<div className="row  justify-content-center align-items-center">
					<div className="col-12 text-left">
						<a
							className="catagory-collapse d-flex justify-content-between"
							data-bs-toggle="collapse"
							href="#"
							role="button"
							aria-expanded="false"
							aria-controls="multiCollapseExample1"
							onClick={toggleOpen}
						>
							<h4 className="d-inline text-uppercase">{question}</h4>
							<FAQButtonContainer>
								{isAllergen && <FmdBadOutlinedIcon />}
								<i
									className={`fas fa-sort-up arrow  ${isOpen && "down"}`}
								></i>{" "}
							</FAQButtonContainer>
						</a>
					</div>
				</div>

				<div className="row">
					<div className="col">
						<div
							className={`collapse multi-collapse ${isOpen && "show"}`}
							id="multiCollapseExample1"
						>
							<div className="card card-body">
								<p>{answer}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default FAQItem;
