import { useState } from "react";
import styled from "styled-components";

const SmoothWrapper = styled.div`
	.smooth-image {
		transition: opacity 1s;
	}
	.image-visible {
		opacity: 1;
	}
	.image-hidden {
		opacity: 0;
	}
`;

const SmoothImage = ({ src, width }) => {
	const [imageLoaded, setImageLoaded] = useState(false);

	return (
		<SmoothWrapper className="smooth-image-wrapper">
			<img
				src={src}
				alt={"Scent Image"}
				className={`smooth-image image-${imageLoaded ? "visible" : "hidden"}`}
				onLoad={() => setImageLoaded(true)}
				style={{ width: `${width}vw` }}
			/>
			{!imageLoaded && (
				<div className="smooth-preloader">
					<span className="loader" />
				</div>
			)}
		</SmoothWrapper>
	);
};

export default SmoothImage;
