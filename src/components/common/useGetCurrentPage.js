import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router";

const useGetCurrentPage = () => {
	const history = useHistory();
	const location = useLocation();

	const urlBlendCheck = () => history.location.pathname.includes("/blends");
	const urlOverviewCheck = () =>
		history.location.pathname.includes("/scenthesiser");
	const urlBottleCheck = () => history.location.pathname.includes("/bottle");

	const [isBlendsPage, setIsBlendPage] = useState(urlBlendCheck());

	const [isOverviewPage, setIsOverviewPage] = useState(urlOverviewCheck());

	const [isBottlePage, setIsBottlePage] = useState(urlOverviewCheck());

	useEffect(() => {
		setIsOverviewPage(urlOverviewCheck);
		setIsBlendPage(urlBlendCheck);
		setIsBottlePage(urlBottleCheck);
	}, [location]);

	return [isBlendsPage, isOverviewPage, isBottlePage];
};

export default useGetCurrentPage;
