import BottleImg from "@Img/bottleIt.png";
import { Link } from "react-router-dom";
import { StyledButton } from "../StyledComponents";

const BottleIt = ({ mobile = false }) => {
	return mobile ? (
		<Link to="/bottle/confirm" id="mixItDesktop">
			<StyledButton dark className=" ">
				Bottle it
			</StyledButton>
		</Link>
	) : (
		<Link to="/bottle/confirm" className="d-block d-lg-none">
			<img id="bottleIt" src={BottleImg} />
		</Link>
	);
};

export default BottleIt;
