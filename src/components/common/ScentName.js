const ScentName = ({ scentName, setScentName, lightText }) => {
	return (
		<div id="nameScent" className={`row py-3 `}>
			<div className="col-12 text-center">
				<h3 className={`mb-0 scent-name ${lightText ? "text-white" : ""}`}>
					Give your perfume a name
				</h3>
				<input
					type="text"
					id="perfumeName"
					name="perfumeName"
					className={` ${lightText ? "text-white" : ""}`}
					placeholder="Type name here+"
					value={scentName}
					onChange={(ev) => setScentName(ev.target.value)}
				/>
			</div>
		</div>
	);
};

export default ScentName;
