import React from "react";
import {
	selectSelectedExtract,
	selectSelectedYeast,
	selectSelectedHop,
	selectInstructions,
} from "../../redux/builtRecipe";
import { useSelector } from "react-redux";
import sugarImg from "../../assets/img/sugar.png";
import AmountOfPints from "./AmountOfPints";

const RecipeIngredients = () => {
	const recipe = useSelector(selectInstructions);
	const extract = useSelector(selectSelectedExtract);
	const hop = useSelector(selectSelectedHop);
	const yeast = useSelector(selectSelectedYeast);
	const {
		hops: recipeHops,
		yeast: recipeYeast,
		extract: recipeExtract,
	} = recipe;
	return (
		<div className="row instruction-selection justify-content-center">
			<AmountOfPints />
			<div className="col-12 mb-4">
				<h2>Your ingredients</h2>
			</div>

			<div className="recipe-option-area row">
				{extract &&
					extract.map((extract, i) => (
						<div key={i} className="col-12 col-md-6 recipe-option mb-4 row">
							<div className="col-4">
								<img
									alt="alt text"
									className="img-fluid w-100"
									src={extract.img}
								/>
							</div>
							<div className="col-8 ps-5">
								<div className="row">
									<div className="col-8">
										<h4>{extract.label}</h4>
									</div>

									<div className="col-12 mt-3">
										<p>
											Quantity:{" "}
											{recipeExtract &&
												recipeExtract.find((ex) => ex.name === extract.label)
													.qty}{" "}
											cans
										</p>
									</div>
								</div>
							</div>
						</div>
					))}
				{yeast &&
					yeast.map((yeast, i) => (
						<div key={i} className="col-12 col-md-6 recipe-option mb-4 row">
							<div className="col-4">
								<img
									alt="alt text"
									className="img-fluid w-100"
									src={yeast.img}
								/>
							</div>
							<div className="col-8 ps-5">
								<div className="row">
									<div className="col-8">
										<h4>{yeast.label}</h4>
									</div>

									<div className="col-12 mt-3">
										<p>
											Quantity:{" "}
											{recipeYeast &&
												recipeYeast.find((ye) => ye.name === yeast.label)
													.qty}{" "}
											sachets
										</p>
									</div>
								</div>
							</div>
						</div>
					))}
				{hop &&
					hop.map((hop, i) => (
						<div key={i} className="col-12 col-md-6 recipe-option mb-4 row">
							<div className="col-4">
								<img alt="alt text" className="img-fluid w-100" src={hop.img} />
							</div>
							<div className="col-8 ps-5">
								<div className="row">
									<div className="col-8">
										<h4>{hop.label}</h4>
									</div>

									<div className="col-12 mt-3">
										<p>
											Quantity:{" "}
											{recipeHops &&
												recipeHops.find((ho) => ho.name === hop.label).qty}{" "}
											sachets
										</p>
									</div>
								</div>
							</div>
						</div>
					))}
				{recipe && recipe.ingredients && recipe.ingredients.amountOfSugar && (
					<div className="col-12 col-md-6 recipe-option mb-4 row">
						<div className="col-4">
							<img alt="alt text" className="img-fluid w-100" src={sugarImg} />
						</div>
						<div className="col-8 ps-5">
							<div className="row">
								<div className="col-8">
									<h4>Sugar</h4>
								</div>

								<div className="col-12 mt-3">
									<p>Quantity: {recipe.ingredients.amountOfSugar}</p>
								</div>
							</div>
						</div>
					</div>
				)}
			</div>
		</div>
	);
};

export default RecipeIngredients;
