import styled, { css } from "styled-components";

export const StyledButton = styled.button`
	border: none;
	padding: 0;
	font: inherit;
	cursor: pointer;
	outline: inherit;
	border-radius: 30px;
	border: 2px solid #fff;
	padding: 0.5rem 1rem;
	margin: 2rem auto 2rem;
	white-space: nowrap;

	${({ dark }) =>
		dark
			? css`
					color: #000;
					background: #fff;
					font-weight: 500;
			  `
			: css`
					color: inherit;
					background: rgba(255, 255, 255, 0.1);
			  `}

	@media (max-width: 768px) {
		margin: 0.4rem auto;
	}
`;
