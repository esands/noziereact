import StartOver from "./StartOver";
import JoyRide from "./JoyRide";

const ModalContent = ({ type }) => {
	if (type === "startOver") return <StartOver />;
};

export default ModalContent;
