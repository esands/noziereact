import styled, { css } from "styled-components";
import useWindowDimensions from "components/common/useWindowDimensions";
import NozieLogo from "@Img/nozieLogo2.png";
import Joyride from "react-joyride";

const ToolTipWrapper = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-items: center;
	background: #fff;
	padding: 1rem;
	border-radius: 4px;

	${({ reversed }) =>
		reversed
			? css`
					border-bottom: 4px solid #000;
			  `
			: css`
					border-top: 4px solid #000;
			  `}
`;

const ButtonContainer = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	gap: 1rem;
	margin-bottom: 1rem;
`;

const JoyRide = () => {
	const { width } = useWindowDimensions();
	let reverseOverlaySteps = [1, 3, 4];

	if (width < 992) {
		// on mobile do not reverse overlay for mix it button
		reverseOverlaySteps = [1, 4];
	}
	const steps = [
		{
			target: "#nameScent",
			title: "Quick guide: Naming your perfume",
			content:
				"Click on the ‘type name here+’ input above to get started. You can edit it your name any time, so you can play around with options.",
			disableBeacon: true,
			offset: 0,
		},
		{
			target: "#blendlist",
			title: "Quick guide: building your perfume",
			content:
				"Create your perfume by adding blends together using the controls below. You can add blends, adjust concentrations, delete blends and start over.",

			disableBeacon: true,
			offset: 0,
		},
		{
			target: "#joyrideNav",
			title: "Quick guide: control your experience",
			content:
				"Use the toolbar at the top to toggle uncover each aspect of your perfume. You can see data, FAQs, ingredients and allergen information here.",

			disableBeacon: true,
		},
		{
			target: width > 992 ? "#mixItDesktop" : "#bottleIt",
			title: "Quick guide: create your perfume",
			content:
				"When you’re done choosing your blends, click the mix it icon to create your perfume and move on to customise your label and bottle and checkout.",

			disableBeacon: true,
		},
		{
			target: "#editConcentration",
			title: "Quick guide: Edit your perfume",
			content:
				"Click on the ‘type name here+’ input above to get started. You can edit it your name any time, so you can play around with options.",

			disableBeacon: true,
		},
	];

	const handleJoyrideCallback = (data) => {
		const { action, index, status, type } = data;
		let overlay = document.querySelector(".react-joyride__overlay");
		if (overlay) {
			overlay = document.querySelector(".react-joyride__overlay");
			if (reverseOverlaySteps.includes(index)) {
				overlay.classList.add("reverse-overlay");
			} else {
				overlay.classList.remove("reverse-overlay");
			}
		}

		if (data?.lifecycle === "complete") {
			localStorage.setItem( 'isTourComplete', "false" )
		}
	};

	return (
		<Joyride
			reverseOverlaySteps={reverseOverlaySteps}
			steps={steps}
			run={localStorage.getItem( 'isTourComplete' ) === "false" ? false : true}
			tooltipComponent={Tooltip}
			disableScrolling
			callback={handleJoyrideCallback}
			styles={{
				options: {
					arrowColor: "#000000",
					backgroundColor: "#fff",
					primaryColor: "#000",
					textColor: "#000",
					overlayColor: "transparent",
					width: 900,
					zIndex: 1000,
				},
				overlay: {
					height: "100vh",
					position: "fixed",
				},
			}}
		/>
	);
};

const Tooltip = ({
	continuous,
	index,
	step,
	backProps,
	skipProps,
	primaryProps,
	tooltipProps,
	reverseOverlaySteps,
}) => {
	return (
		<ToolTipWrapper
			{...tooltipProps}
			reversed={reverseOverlaySteps?.includes(index)}
		>
			<img src={NozieLogo} alt="" />
			{step.title && <h3>{step.title}</h3>}
			<p class="w-75 w-md-50 text-center">{step.content}</p>
			<ButtonContainer>
				{index > 0 && (
					<button {...backProps} className="btn-simple">
						&#60; Back
					</button>
				)}
				{index < 4 && (
					<button {...primaryProps} className="btn-simple">
						Next &#62;
					</button>
				)}
			</ButtonContainer>

			<button {...skipProps} className="btn-simple">
				Ok, got it thanks, finish guide
			</button>
		</ToolTipWrapper>
	);
};

export default JoyRide;
