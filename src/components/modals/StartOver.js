import styled from "styled-components";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setIsModalOpen } from "@Redux/app";

const ModalWrapper = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-items: center;
`;

const StartOver = () => {
	const dispatch = useDispatch();
	const closeModal = () => {
		dispatch(setIsModalOpen(false));
	};
	return (
		<ModalWrapper>
			<h3>Just a quick check</h3>
			<p>
				Are you sure you would like to start over? This will delete all the
				blends and settings from this session.
			</p>
			<Link
				to="/"
				// onClick={() => dispatch(setNextBlendValueToAdd(100))}
				className="primary primary-btn mb-2"
				onClick={closeModal}
			>
				Yes Start Over
			</Link>

			<a onClick={closeModal} className="secondary secondary-btn">
				Cancel
			</a>
		</ModalWrapper>
	);
};

export default StartOver;
