import styled from "styled-components";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setIsModalOpen } from "@Redux/app";
import { useState } from "react";
import Joyride from "react-joyride";

const ModalWrapper = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-items: center;
`;

const Wizard = () => {
	const dispatch = useDispatch();
	const closeModal = () => {
		dispatch(setIsModalOpen(false));
	};

	const [step, setStep] = useState(1);
	const steps = [
		{
			target: ".my-first-step",
			content: "This is my awesome feature!",
		},
		{
			target: ".my-other-step",
			content: "This another awesome feature!",
		},
	];
	const renderSwitch = (step) => {
		switch (step) {
			case 1:
				return <Step1 closeModal={closeModal} setStep={setStep} />;
			case 2:
				return <Step2 />;
			case 3:
				return <Step3 />;
		}
	};

	return <ModalWrapper>{renderSwitch(step)}</ModalWrapper>;
};

export default Wizard;

const Step1 = ({ setStep, closeModal }) => {
	return (
		<>
			<h3>Just a quick check</h3>
			<p>
				Are you sure you would like to start over? This will delete all the
				blends and settings from this session.
			</p>
			<a
				href="#"
				onClick={() => setStep(2)}
				className="primary primary-btn mb-2"
			>
				Next
			</a>

			<a onClick={closeModal} className="secondary secondary-btn">
				Ok, got it thanks, finish guide
			</a>
		</>
	);
};

const Step2 = () => {
	return <div>Wizard</div>;
};

const Step3 = () => {
	return <div>Wizard</div>;
};
