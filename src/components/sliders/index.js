import { useState, useEffect } from "react";
import Slider from "./Slider";
import SliderButton from "./SliderButton";
import styled, { css } from "styled-components";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { removeSelectedBlend, setSelectedBlends, selectSelectedBlends, } from "@Redux/builtBlend";
import { StyledButton } from "components/StyledComponents";
import BottleIt from "components/common/BottleIt";

const SliderContainer = styled.div`
	display: flex;
	overflow-y: auto;

	${(props) =>
		props.sliderCount > 2
			? css`
					height: 23rem;
			  `
			: css`
					height: unset;
			  `}

	${(props) =>
		!props.isStandardLayout
			? css`
					flex-direction: column-reverse;
			  `
			: css`
					height: 500px;
			  `}

	input {
		display: block;
		margin-left: 2rem;
		${(props) =>
			props.isStandardLayout &&
			css`
				transform: rotate(270deg);
			`}
	}

	::-webkit-scrollbar {
	width: 5px;
	}

	::-webkit-scrollbar-track {
	background: rgba(255,255,255,0.1);
	}

	::-webkit-scrollbar-thumb {
	background: #fff;
	border-radius: 30px
	}

	::-webkit-scrollbar-thumb:hover {
	background: #fff;
	}

	::-moz-scrollbar {
		width: 5px;
	}

	::-moz-scrollbar-track {
		background: rgba(255,255,255,0.1);
	}

	::-moz-scrollbar-thumb {
		background: #fff;
		border-radius: 30px
	}

	::-moz-scrollbar-thumb:hover {
		background: #fff;
	}
`;

const SlidersWrapper = styled.div`
	transition: all 0.4s ease-out;
	color: #fff;
	height: 100vh;
	${({ showSliders }) =>
		showSliders &&
		css`
			background: rgba(30, 212, 197, 0.7);
		`};
`;

const ErrorMessage = styled.p`
	position: absolute;
	text-align: center;
	left: 50%;
	transform: translateX(-50%);
`;

//create your forceUpdate hook
function useForceUpdate() {
	const [value, setValue] = useState(0); // integer state
	return () => setValue((value) => value + 1); // update the state to force render
}

function Sliders({ handleShowSlider, showSliders }) {
	const selectedBlends = useSelector(selectSelectedBlends);
	const selectedBlendValues = selectedBlends.map(({ value }) => value);
	const initialBlendSliders = selectedBlends.map((blend) => false);
	const [sliderValue, setSliderValue] = useState(selectedBlendValues);
	const [lockedSliders, setLockedSliders] = useState(initialBlendSliders);
	const [total, setTotal] = useState(100);
	const [isStandardLayout, setIsStandardLayout] = useState(false);
	const [error, setError] = useState("");
	const [lastAdjusted, setLastAdjusted] = useState(1);
	const [hideNumbers, setHideNumbers] = useState(false);
	const [showDelete, setShowDelete] = useState(false);
	const forceUpdate = useForceUpdate();
	const dispatch = useDispatch();
	const onConfirmationClick = () => {
		handleShowSlider();
		dispatch(setSelectedBlends(sliderValue));
	};
	useEffect(() => {
		const newTotal = sliderValue.reduce((acc, curr, i) => acc + curr, 0);
		setTotal(newTotal);
	}, [sliderValue]);

	const deleteButton = (index) => {
		setSliderValue((state) => {
			const valueToDistribute = state[index];
			const withValueRemoved = [...state.filter((val, i) => i !== index)];
			const sliderCount = sliderValue.length;
			const totalToDistribute = Math.floor(
				valueToDistribute / (sliderCount - 1)
			);
			const totalToAddToFoundation = valueToDistribute % (sliderCount - 1);
			return [
				...withValueRemoved.map((slider, i) => {
					if (i === 0)
						return slider + totalToDistribute + totalToAddToFoundation;

					return slider + totalToDistribute;
				}),
			];
		});

		setLockedSliders((state) => [...state.filter((val, i) => i !== index)]);
		dispatch(removeSelectedBlend(index));
		forceUpdate();
	};

	return (
		<SlidersWrapper
			showSliders={showSliders}
			className={`fixed-bottom remove-z-index bottomGradient d-flex justify-content-end flex-column align-items-center  ${
				!isStandardLayout ? "horizontal-layout" : ""
			} `}
		>
			{" "}
			{showSliders && (
				<div class="mt-5">
					<div className="d-flex space-between mt-5">
						<div className="add-button">
							<SliderButton
								text="Add Blend +"
								setError={setError}
								setSliderValue={setSliderValue}
								sliderCount={sliderValue.length}
								isIncrement
								sliderValue={sliderValue}
								total={total}
								setTotal={setTotal}
								setLockedSliders={setLockedSliders}
								setLastAdjusted={setLastAdjusted}
								isLockedArray={lockedSliders}
							/>
						</div>
						{/* <div className="">
							<SliderButton
								text="Delete Blend -"
								setError={setError}
								setSliderValue={setSliderValue}
								sliderCount={sliderValue.length}
								sliderValue={sliderValue}
								total={total}
								setTotal={setTotal}
								setLockedSliders={setLockedSliders}
								setLastAdjusted={setLastAdjusted}
								isLockedArray={lockedSliders}
								setShowDelete={setShowDelete}
								showDelete={showDelete}
							/>
						</div> */}
					</div>
					<SliderContainer isStandardLayout={isStandardLayout} sliderCount={sliderValue.length}>
						{sliderValue.map((slider, i) => (
							<Slider
								key={i}
								total={total}
								setTotal={setTotal}
								index={i}
								sliderCount={sliderValue.length}
								sliderValue={slider}
								setSliderValue={setSliderValue}
								slidersArray={sliderValue}
								setLastAdjusted={setLastAdjusted}
								lastAdjusted={lastAdjusted}
								setLockedSliders={setLockedSliders}
								isLocked={lockedSliders[i]}
								isLockedArray={lockedSliders}
								setError={setError}
								hideNumbers={hideNumbers}
								isStandardLayout={isStandardLayout}
								forceUpdate={forceUpdate}
								showDelete={showDelete}
								deleteButton={(index) => deleteButton(index)}
							/>
						))}
					</SliderContainer>
					{error && <ErrorMessage> {error}</ErrorMessage>}
					{sliderValue.length > 4 && <p>This many blends may get funky</p>}
					<div className="d-flex justify-content-center">
						<StyledButton onClick={onConfirmationClick}>
							Set Concentrations
						</StyledButton>
					</div>
				</div>
			)}
			{!showSliders && (
				<>
					{/* <BlendList
						sliderValue={sliderValue}
						handleShowSlider={handleShowSlider}
						showDelete={showDelete}
						deleteButton={(index) => deleteButton(index)}
					/> */}

					<div id="addRemoveRestart" className="pb-3">
						<div className="container">
							<div className="d-flex align-items-center gap-3 justify-content-center text-center flex-row">
							<div className="">
									<StyledButton id="editConcentration" onClick={handleShowSlider}>
										Edit Perfume 
									</StyledButton>
								</div>
								<BottleIt mobile />
							</div>
						</div>
					</div>
				</>
			)}
		</SlidersWrapper>
	);
}

export default Sliders;
