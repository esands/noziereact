import styled, { css } from "styled-components";
import CloseIcon from "@mui/icons-material/Close";
import { useSelector } from "react-redux";
import TinySlider from "tiny-slider-react";
import "tiny-slider/dist/tiny-slider.css";
import { useRef } from "react";

function capitalizeFirstLetter(string) {
	const words = string.split(" ");

	const capitalized = words
		.map(
			(string) => string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
		)
		.join(" ");
	return capitalized;
}

const settings = {
	items: 1,
	center: true,
	fixedWidth: 300,
	loop: false,
	slideBy: 1,
	mouseDrag: false,
	controls: true,
	nav: false,
	gutter: 30,
	responsive: {
		375: {
			items: 2,
			fixedWidth: 220,
			mouseDrag: true,
		},
		900: {
			items: 1,
			fixedWidth: 300,
			mouseDrag: true,
		},
	},
};

// {imgs.map((el, index) => (
// 	<div key={index} style={{ position: "relative" }}>
// 		<img
// 			className={`tns-lazy-img`}
// 			src={loadingImage}
// 			data-src={el}
// 			alt=""
// 			style={imgStyles}
// 		/>
// 	</div>
// ))}

const BlendContainer = styled.div`
	& .tns-controls {
		display: flex;
		justify-content: center;
	}
`;

const BlendItem = styled.div`
	position: relative;
	button:not(#isLockedAt50) {
		${(props) =>
			props.showDelete
				? css`
						position: absolute;
						left: -6px;
						top: 5px;
						border-radius: 50%;
						width: 35px;
						height: 35px;
						border: none;
						background: rgb(33, 37, 41);
						color: #fff;
						padding: 0;
				  `
				: css`
						display: none;
						pointer-events: none;
				  `}
	}
	.blendTitle,
	.blendPercentage {
		white-space: nowrap;
		color: #000;
	}

	.blendTitle {
		font-size: 1rem;
	}

	.blendPercentage {
		font-size: 1.5rem;
	}
`;

const BlendList = ({ sliderValue, handleShowSlider, deleteButton }) => {
	const sliderRef = useRef(null);
	return (
		<div id="blendlist" className="">
			<BlendContainer className="container-fluid">
				<div className="row py-3">
					<div className="col-12 p-0">
						{/* <BlendContainer className="center" id="center"> */}

						<TinySlider settings={settings} ref={sliderRef}>
							{" "}
							{sliderValue.map((slider, i) => (
								<Blend
									handleShowSlider={handleShowSlider}
									slider={slider}
									key={i}
									index={i}
									deleteButton={deleteButton}
									sliderRef={sliderRef}
								/>
							))}
						</TinySlider>
						{/* </BlendContainer> */}
					</div>
				</div>
			</BlendContainer>
		</div>
	);
};

const Blend = ({
	slider,
	handleShowSlider,
	index,

	deleteButton,
	sliderRef,
}) => {
	const blend = useSelector((state) => state.builtBlend.selectedBlends[index]);
	const showDelete = useSelector((state) => state.builtBlend.showDelete);
	const onClickHandler = () => {
		deleteButton(index);
	};
	return (
		<BlendItem
			className="item tns-item tns-slide-active"
			showDelete={showDelete}
		>
			<div className="blendButton text-center">
				<h2>{index === 0 ? "Base" : `Accent ${index + 1}`}</h2>
				<div
					onMouseUpCapture={handleShowSlider}
					className="blendTab d-flex align-items-center"
				>
					<div className="blendImage me-2"></div>
					<div className="blendInfo">
						<div className="blendPercentage">{slider}%</div>
						<div className="blendTitle">
							{capitalizeFirstLetter(blend?.scentName)}
						</div>
					</div>
				</div>
			</div>{" "}
			<button
				className={!showDelete ? "test" : undefined}
				onClick={onClickHandler}
			>
				<CloseIcon style={{ color: "#fff" }} />
			</button>
		</BlendItem>
	);
};

export default BlendList;
