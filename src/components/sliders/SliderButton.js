import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import {
	setSelectedBlends,
	setNextBlendValueToAdd,
	setShowDeleteAction,
} from "@Redux/builtBlend";
import styled from "styled-components";
import { StyledButton } from "@Components/StyledComponents";

const SliderButton = ({
	setError,
	sliderCount,
	text,
	isIncrement,
	sliderValue,
	setTotal,
	setLockedSliders,
	setLastAdjusted,
	setSliderValue,
	isLockedArray,
	setShowDelete,
	showDelete,
}) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const setNewSliderValue = () => {
		const valueMap = {
			0: 100,
			1: 20,
			2: 20,
		};

		if (sliderCount <= 2) {
			return valueMap[sliderCount];
		}

		const foundationBlend = 50;

		const totalWithoutFoundation = sliderValue.reduce((acc, curr, i) => {
			return acc + (i === 0 ? 0 : curr);
		}, 0);

		// const totalToDistribute = Math.floor(totalWithoutFoundation / sliderCount);
		// const totalToAddToFoundation = totalWithoutFoundation % sliderCount;
		const totalToDistribute = Math.floor(foundationBlend / sliderCount);
		const totalToAddToFoundation = foundationBlend % sliderCount;

		setSliderValue((state) => {
			return [
				...state.map((slider, i) => {
					if (i === 0) return foundationBlend + totalToAddToFoundation;

					return totalToDistribute;
				}),
			];
		});

		return 10;
	};

	const onAddSlider = () => {
		if (sliderCount == 0) {
			dispatch(setNextBlendValueToAdd(100));
		} else if (sliderCount <= 2) {
			dispatch(setNextBlendValueToAdd(setNewSliderValue()));
		} else {
			dispatch(setNextBlendValueToAdd(Math.floor(50 / sliderCount)));
		}

		if (sliderCount <= 2) {
			const tempValue = sliderValue;
			const newState = [tempValue[0], setNewSliderValue()];
			setSliderValue(newState);
			setLockedSliders((state) => [...state, false]);
			setLastAdjusted(1);
			dispatch(setSelectedBlends(newState));

			history.push("/blends");
			return;
		}

		setSliderValue((state) =>
			isIncrement ? [...state, 0] : [...state.slice(0, state.length - 1)]
		);
		setNewSliderValue();
		setLockedSliders((state) => [...state, false]);
		setLastAdjusted(1);

		const foundationBlend = 50;
		const totalToDistribute = Math.floor(foundationBlend / sliderCount);
		const totalToAddToFoundation = foundationBlend % sliderCount;

		const updatedState = sliderValue.map((slider, i) => {
			if (i === 0) return foundationBlend + totalToAddToFoundation;

			return totalToDistribute;
		});

		dispatch(setSelectedBlends(updatedState));
		history.push("/blends");
	};

	const onRemoveSlider = () => {
		setShowDelete(!showDelete);
		dispatch(setShowDeleteAction(!showDelete));
	};
	return (
		<StyledButton
			onClick={() => {
				if (isIncrement ? sliderCount === 20 : sliderCount === 1) {
					isIncrement
						? setError("Maximum 20 blends")
						: setError("Minimum 1 blends");
					setTimeout(() => setError(""), 5000);
				} else {
					isIncrement ? onAddSlider() : onRemoveSlider();
				}
			}}
		>
			{text}
		</StyledButton>
	);
};

export default SliderButton;
