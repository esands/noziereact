import { useState, useRef } from "react";
import styled, { css } from "styled-components";
import { DeleteOutlineOutlined } from "@material-ui/icons";
import { useDispatch, useSelector } from "react-redux";

const BlendInfo = styled.div`
	color: #fff;
	display: flex;
	flex-direction: column;
	align-items: center;
	padding-left: 1rem;

	& .blendPercentage {
		font-size: 1.4rem;
	}

	& .blendTitle {
		font-size: 0.8rem;
		position: absolute;
		top: -25px;
		white-space: nowrap;
	}
`;

const SliderDiv = styled.div`
	margin-right: 1rem;

	& .blendTab {
		border-radius: 50%;
		display: grid;
		border: none;
		place-content: center;
		padding: 0;
		margin-left: 0.5rem;
		cursor: pointer;
		background: transparent;

		svg {
			width: 30px;
			height: 30px;
		}
	}

	${(props) =>
		props.isStandardLayout
			? css`
					flex-direction: column;
			  `
			: css`
					align-items: center;
					position: relative;
					margin-bottom: 1rem;
			  `}

	h5 {
		${(props) =>
			props.isStandardLayout
				? css`
						margin-bottom: 6rem;
				  `
				: css`
						position: absolute;
						left: -30px;
				  `}
	}

	button#isLockedAt50 {
		position: absolute;
		left: -200px;
	}
`;

function NozieSlider({
	setTotal,
	index,
	total,
	sliderCount,
	sliderValue,
	setSliderValue,
	slidersArray,
	lastAdjusted,
	setLastAdjusted,
	isLocked,
	isLockedArray,
	setLockedSliders,
	setError,
	hideNumbers,
	isStandardLayout,
	forceUpdate,
	showDelete,
	deleteButton,
}) {
	const SliderMap = {
		0: 100,
		1: 20,
		2: 20,
	};
	const dispatch = useDispatch();

	const [isLockedAt50, setIsLockedAt50] = useState(true);
	const checkOthersZeroOrLocked = (state) => {
		let newState = [...state];

		newState.splice(index, 1);
		if (index != 0) newState.shift();
		const tempLocked = [...isLockedArray];
		tempLocked.splice(index, 1);
		if (index != 0) tempLocked.shift();

		return newState.every((slider, i) => slider === 0 || tempLocked[i]);
	};

	const checkOthersAllLocked = (state) => {
		const tempLocked = [...isLockedArray];
		tempLocked.splice(index, 1);
		if (index != 0) tempLocked.shift();

		return tempLocked.every((slider, i) => slider);
	};

	const calculateTotalOfNonLockedOthers = (val) => {
		const tempLocked = [...isLockedArray];
		const tempSliderValues = [...slidersArray];
		let total = 0;
		for (var i = 0; i < tempLocked.length; i++) {
			if (!tempLocked[i] && i != index) {
				total += tempSliderValues[i];
			}
		}

		return total;
	};

	const totalNumOfSlidersNotLocked = () => {
		const tempLocked = [...isLockedArray];
		tempLocked.splice(index, 1);
		if (index != 0) tempLocked.shift();

		return tempLocked.filter((slider, i) => !slider).length;
	};

	const handleOnChange = (e) => {

		if (isLocked) {
			return;
		}

		let val = parseInt(e.target.value);
		const id = e.target.id;

		if (id === "foundation" && val < 50 && isLockedAt50) {
			return;
		}

		setSliderValue((state) => {
			let newState = [...state];

			let newDiff = val - newState[index];

			const totalWithoutNewDiff =
				newState.reduce((acc, curr, i) => (i !== index ? acc + curr : acc), 0) +
				val -
				100;

			if (totalWithoutNewDiff > 0) {
				if (totalWithoutNewDiff > calculateTotalOfNonLockedOthers()) {
					const valToSubtract =
						totalWithoutNewDiff - calculateTotalOfNonLockedOthers();
					newDiff = newDiff - valToSubtract;
					val = val - valToSubtract;
				}
			}

			const checkZeroOrLocked = checkOthersZeroOrLocked(newState);
			const checkOthersLocked = checkOthersAllLocked();
			const slidersNotLocked = totalNumOfSlidersNotLocked();
			let applyValueToFirstOnly = false;
			const totalToDistribute = Math.floor(newDiff / (newState.length - 2));
			let totalToAdjustLast =
				Math.ceil(newDiff / (newState.length - 2)) - totalToDistribute;

			if (totalToDistribute === 0) {
				applyValueToFirstOnly = true;
			}

			if (checkZeroOrLocked && newDiff > 0) {
				setError("All others are zero or locked");
				setTimeout(() => {
					setError("");
				}, 3000);
				return newState;
			}

			if (checkOthersLocked) {
				setError("All others are zero or locked");
				setTimeout(() => {
					setError("");
				}, 3000);
				return newState;
			}

			newState[index] = parseInt(val);

			// add to other sliders
			if (newState.length > 1) {
				let skipAdjust = 0;
				let updatedLastAdjusted = lastAdjusted + skipAdjust;
				const directionalCheck = (difference, value) => {
					if (difference > 0) {
						return value !== 0;
					} else {
						return true;
					}
				};

				var offset = lastAdjusted;
				for (var i = 0; i < newState.length; i++) {
					var pointer = (i + offset) % newState.length;
					if (
						pointer !== index &&
						pointer !== 0 &&
						directionalCheck(newDiff, newState[pointer]) &&
						!isLockedArray[pointer]
					) {
						updatedLastAdjusted = pointer;
						break;
					}
				}
				let remainderToRemove = 0;
				if (
					newState[updatedLastAdjusted] >= 0 &&
					!isLockedArray[updatedLastAdjusted]
				) {
					if (newDiff === -1) {
						newState[updatedLastAdjusted] =
							newState[updatedLastAdjusted] + Math.abs(newDiff);
					} else {
						if (newState[updatedLastAdjusted] - newDiff < 0) {
							newState[updatedLastAdjusted] =
								newState[updatedLastAdjusted] - newDiff;
							remainderToRemove += Math.abs(
								newState[updatedLastAdjusted] - newDiff
							);
						} else {
							newState[updatedLastAdjusted] =
								newState[updatedLastAdjusted] - newDiff;
						}
					}

					if (newState[updatedLastAdjusted] < 0) {
						newState[updatedLastAdjusted] = 0;
					}
				}

				let loopIndex = 0;
				let loopCount = 0;
				//While the total is above 100 (only happens due to an error)
				while (
					newState.reduce((partialSum, a) => partialSum + a, 0) - 100 >
					0
				) {
					//Make sure it doesn't change the user changed slider, make sure that the value isn't already zero and make sure it's not locked.
					if (
						loopIndex !== index &&
						newState[loopIndex] > 0 &&
						!isLockedArray[loopIndex]
					) {
						newState[loopIndex] = newState[loopIndex] - 1;
					} else {
						loopIndex++;
					}
					loopCount++;
					if (loopCount === 100) break;
				}

				setLastAdjusted((prev) => {
					return prev === newState.length - 1 ? 1 : prev + 1;
				});
			}

			return newState;
		});
	};

	const lockHandler = () => {
		setLockedSliders((state) => {
			const newState = [...state];
			newState[index] = !isLocked;
			return newState;
		});
	};

	const blend = useSelector((state) => state.builtBlend.selectedBlends[index]);
	const onClickHandler = () => {
		deleteButton(index);
	};
	let clickRef = useRef(false);
	return (
		<SliderDiv
			className=""
			isStandardLayout={isStandardLayout}
			showDelete
			isLocked={isLocked}
		>
			<div className="mb-2 d-flex align-center w-100 space-between">
				<p className="mb-0 accent-layer me-4">
					{index === 0 ? "Base" : "Accent "+index }
				</p>
				<hr className="w-100"/>
				<button
					className="blendTab"
					onClick={onClickHandler}
				>
					<span class="deleteIcon"></span>
				</button>
			</div>
			<div className="blendTitle">{blend?.scentName}</div>
			<div className="d-flex align-center">
				<div className={`blendImage blendList ${isLocked ? "locked" : ""}`}></div>
				<input
					type="range"
					className={`slido range ${isLocked ? "locked" : "unlocked"}`}
					id={index === 0 ? "foundation" : "subsequent"}
					name={index === 0 ? "foundation" : "subsequent"}
					min="0"
					max="100"
					value={sliderValue}
					onInput={(e) => {
						handleOnChange(e);
						clickRef.current = true;
					}}
					onClick={e => {
						if (!clickRef.current) lockHandler();
						clickRef.current = false;
					}}
					step="1"
				/>

				{index === 0 && (
					<button
						id="isLockedAt50"
						onClick={() => setIsLockedAt50(!isLockedAt50)}
					>
						{isLockedAt50 ? "Unlock from 50" : "Lock from 50"}
					</button>
				)}
				<BlendInfo>
					<div className="d-flex align-center concentration">
						<div className="d-flex flex-column align-center">
							<p>{isLocked ? "Locked" : "Unlocked"}</p>
							<div className="blendPercentage">{sliderValue}% </div>
							<p>Concentration</p>
						</div>
					</div>
				</BlendInfo>
			</div>
		</SliderDiv>
	);
}

export default NozieSlider;
