import { useEffect, useState } from "react";
import { Collapse } from "react-collapse";
import { useDispatch, useSelector } from "react-redux";
import {
	addCategoryFilter,
	addCategoryAndChildrenFilter,
} from "../../redux/blends";
import BlendImg from "../../img/blendImage.png";

const Category = ({ category }) => {
	const dispatch = useDispatch();
	const [isOpen, setIsOpen] = useState(false);
	const toggleIsOpen = () => {
		setIsOpen(!isOpen);
	};
	const onClickHandler = () => {
		dispatch(addCategoryAndChildrenFilter(category));
	};

	const filters = useSelector((state) => state.blends.categoryFilter);
	const [isChecked, setIsChecked] = useState(false);
	useEffect(() => {
		setIsChecked(filters.includes(category.name));
	}, [filters]);
	return (
		<div className="col-md-8 offset-md-2 col-12">
			<div className="flex-row">
				<div className="col catagory">
					<div className="row align-items-center">
						<div className="col-8 text-left">
							<a
								className="catagory-collapse"
								data-bs-toggle="collapse"
								href="#multiCollapseExample1"
								role="button"
								aria-expanded="false"
								aria-controls="multiCollapseExample1"
								onClick={toggleIsOpen}
							>
								<i className={`fas fa-sort-up arrow ${isOpen && "down"}`}></i>
								{category?.name}
							</a>
						</div>
						<div className="col">
							<div className="form-check select-all">
								<label className="form-check-label" htmlFor="check1">
									Select all
								</label>
								<input
									className="form-check-input-custom"
									type="checkbox"
									id="check1"
									name="option1"
									checked={isChecked}
									onChange={onClickHandler}
								/>
							</div>
						</div>
					</div>
					<p>
						Lorem ipsum dolor sit amet, fugit deleniti in nec. Ius prompta
						democritum ut.
					</p>
					<Collapse isOpened={isOpen}>
						<div className="row">
							<div className="col">
								<div className="card card-body">
									{category?.subCats?.map((subCat, i) => (
										<SubCategory subCat={subCat} filters={filters} key={i} />
									))}
								</div>
							</div>
						</div>
					</Collapse>
				</div>
			</div>
		</div>
	);
};

export default Category;

const SubCategory = ({ subCat, filters }) => {
	const dispatch = useDispatch();
	const onClickHandler = () => {
		dispatch(addCategoryFilter(subCat));
	};
	const [isChecked, setIsChecked] = useState(false);
	useEffect(() => {
		setIsChecked(filters.includes(subCat));
	}, [filters]);
	return (
		<div className="row my-2">
			<div className="col-4">
				<img src={BlendImg} className="h-100 w-100" alt="" />
			</div>
			<div className="col-8">
				<h2>{subCat}</h2>
				<p>
					Lorem ipsum dolor sit amet, fugit deleniti in nec. Ius prompta
					democritum ut.
				</p>
				<div className="form-check select-one">
					<input
						className="form-check-input-custom"
						type="checkbox"
						id="check1"
						name="option1"
						checked={isChecked}
						onChange={onClickHandler}
					/>
				</div>
			</div>
		</div>
	);
};
