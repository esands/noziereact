import styled, { css } from "styled-components";
import CloseIcon from "@mui/icons-material/Close";
import {
	selectFilteredBySearchTerm,
	addCategoryFilter,
	addMoodFilter,
	clearAllFilters,
} from "@Redux/blends";
import { useDispatch, useSelector } from "react-redux";
import { motion, AnimatePresence } from "framer-motion";

const ClearAllButtom = styled.button`
	display: flex;
	justify-content: space-between;
	gap: 1rem;
	margin-bottom: 1rem;
`;

const ApppliedFiltersList = styled.ul`
	display: flex;
	padding-left: 0;
	list-style: none;
	flex-wrap: wrap;
`;

const ApppliedFiltersListItem = styled(motion.li)`
	display: flex;
	justify-content: center;
	gap: 2rem;
	align-items: center;
	border: 1px solid #000;
	position: relative;
	padding: 0.1rem 4rem;
	border-radius: 15px;
	margin-bottom: 0.5rem;
	margin-right: 1rem;

	svg {
		${({ isListItem }) =>
			isListItem &&
			css`
				position: absolute;
				left: 10px;
				top: 5px;
				cursor: pointer;
			`}
	}
`;

const AppliedFilters = () => {
	const blendsFilteredBySearchTerm = useSelector(selectFilteredBySearchTerm);
	const moodFilters = useSelector((state) => state.blends.moodFilter);
	const categoryFilters = useSelector((state) => state.blends.categoryFilter);
	const filterType = useSelector((state) => state.blends.filterType);
	const showFilters = filterType !== "search" && filterType !== "all";
	const filtersMap = {
		category: categoryFilters,
		mood: moodFilters,
	};

	const dispatch = useDispatch();
	const onClickHandler = (filter) => {
		if (filterType === "category") {
			dispatch(addCategoryFilter(filter));
		} else if (filterType === "mood") {
			dispatch(addMoodFilter(filter));
		}
	};

	const onClearAll = () => {
		dispatch(clearAllFilters());
	};
	return (
		showFilters && (
			<div className="col-12">
				<ClearAllButtom className="button--reset">
					Clear All <CloseIcon onClick={onClearAll} />
				</ClearAllButtom>
				<ApppliedFiltersList>
					<AnimatePresence>
						{filtersMap[filterType]?.map((filter, i) => (
							<ApppliedFiltersListItem
								isListItem
								key={i}
								initial={{ opacity: 0, scale: 0 }}
								animate={{ opacity: 1, scale: 1 }}
								exit={{ opacity: 0, scale: 0 }}
							>
								<CloseIcon
									fontSize="small"
									onClick={() => onClickHandler(filter)}
								/>

								<span>{filter}</span>
							</ApppliedFiltersListItem>
						))}
					</AnimatePresence>
				</ApppliedFiltersList>
			</div>
		)
	);
};

export default AppliedFilters;
