import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
	setIsMoodWallFilterOpen,
	addMoodFilter,
	clearAllFilters,
	applyAllFilters,
	selectMoodFilters,
} from "@Redux/blends";
import FilterFooter from "./FilterFooter";
import { selectAppliedFilters } from "../../redux/blends";

const moods = [
	"Playful",
	"Warm",
	"LaidBack",
	"Sexy",
	"Badass",
	"Sophisticated",
	"Soft",
	"Romantic",
	"Quirky",
	"Conventional",
	"Fun",
	"Attention Seeking",
	"Edgy",
	"Crisp",
	"Sweet",
	"Confident",
	"Understated",
	"Sensuous",
	"Calm",
	"Dark",
	"Moody",
	"Vibrant",
	"Colourful",
	"Romantic",
	"Bold",
	"Comforting",
	"Warm",
	"Cool",
	"Fresh",
	"Cool",
	"Uplifting",
	"Bubbly",
	"Deep",
	"Everyday",
	"Evening",
	"Special Occasions",
	"Versatile",
	"Spring",
	"Summer",
	"Autumn",
	"Winter",
	"All Seasons",
	"Fruity",
	"Floral",
	"Spicy",
	"Resinous",
	"Woody",
	"Fresh",
	"Green",
	"Earthy",
	"Animalic",
	"Provocative",
	"Gourmand",
];

const MoodWallFilter = () => {
	const dispatch = useDispatch();

	const onClickHandler = (filter) => {
		dispatch(addMoodFilter(filter));
	};
	const clearHandler = () => {
		dispatch(clearAllFilters());
	};

	const applyHandler = () => {
		dispatch(applyAllFilters());
	};

	const moodFilter = useSelector(selectMoodFilters);
	const appliedFilters = useSelector(selectAppliedFilters);

	return (
		<div className="blend-words">
			<div className="aboveDroplets position-relative">
				<div className="container-md">
					<div className="row justify-content-center align-items-center mt-3">
						<div className="col-12">
							<span
								className="float-start close"
								onClick={() => {
									dispatch(setIsMoodWallFilterOpen(false));
								}}
							>
								&#10005;
							</span>
							<h3 className="text-center">filter by mood, style or occasion</h3>
						</div>
					</div>
					<div className="row justify-content-center align-items-center mt-3">
						<div className="col-7 text-center">
							<h2 className="">
								What kind of blend are you looking for? Select some words to
								help you get started..
							</h2>
						</div>
					</div>

					<div className="row mt-3 justify-content-center">
						<div className="col-12 p-0 text-center">
							<div className="word-list">
								{moods.map((mood) => (
									<Mood
										handler={onClickHandler}
										mood={mood}
										selectedFilters={moodFilter}
									/>
								))}
							</div>
						</div>
					</div>
				</div>
				<FilterFooter count={moodFilter.length} filter="mood" />
			</div>
		</div>
	);
};

export default MoodWallFilter;

const Mood = ({ mood, handler, selectedFilters }) => {
	const isSelected = selectedFilters.includes(mood);
	return (
		<a
			href="#"
			onClick={() => {
				handler(mood);
			}}
			data-title={mood}
			className={`word ${isSelected ? "selected" : ""}`}
		>
			{mood}
		</a>
	);
};
