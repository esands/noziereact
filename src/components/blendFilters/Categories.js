import React, { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import BlendImage from "@Img/blendImage.png";
import {
	setIsCategoryFilterOpen,
	selectBlendCategories,
	selectAppliedFilters,
} from "@Redux/blends";
import Category from "./Category";
import FilterFooter from "./FilterFooter";

const BlendsRange = () => {
	const location = useLocation();
	const dispatch = useDispatch();
	const categories = useSelector(selectBlendCategories);
	const count = [...Array(20)];
	useEffect(() => {}, [dispatch, location.pathname]);

	const categoryFilter = useSelector((state) => state.blends.categoryFilter);
	const appliedFilters = useSelector(selectAppliedFilters);
	return (
		<div className="catagories">
			<div className="aboveDroplets position-relative">
				<div className="container-md">
					<div className="row justify-content-center align-items-center my-3">
						<div className="col-12">
							<span
								className="float-start close"
								onClick={() => {
									dispatch(setIsCategoryFilterOpen(false));
								}}
							>
								&#10005;
							</span>
							<h3 className="text-center">Filter by Catagory</h3>
						</div>
					</div>

					<div className="row mt-4">
						{categories &&
							categories.map((category) => <Category category={category} />)}
					</div>
				</div>
				<FilterFooter count={categoryFilter.length} filter="category" />
			</div>
		</div>
	);
};

export default BlendsRange;
