import { useDispatch } from "react-redux";
import {
	setIsMoodWallFilterOpen,
	addMoodFilter,
	clearAllFilters,
	applyAllFilters,
	selectMoodFilters,
} from "@Redux/blends";

const FilterFooter = ({ count, filter }) => {
	const dispatch = useDispatch();

	const clearHandler = () => {
		dispatch(clearAllFilters());
	};

	const applyHandler = () => {
		dispatch(applyAllFilters(filter));
	};
	return (
		<footer className="sticky-footer">
			<div className="row justify-content-center align-items-center h-100">
				<div className="col-6 text-end">
					<button className="text-uppercase clear" onClick={clearHandler}>
						Clear All
					</button>
				</div>
				<div className="col-6 text-start">
					<button className="text-uppercase apply" onClick={applyHandler}>
						Apply <span id="wordCount">{count}</span> Filters
					</button>
				</div>
			</div>
		</footer>
	);
};

export default FilterFooter;
