import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setIsSearchFilterOpen, setSearchTerm } from "@Redux/blends";

const Search = () => {
	const dispatch = useDispatch();
	const searchTermSelector = useSelector((state) => state.blends.searchTerm);
	const [searchTerm, updateSearchTerm] = useState(searchTermSelector);
	const handleKeyPress = (e) => {
		if (e.key === "Enter") {
			dispatch(setSearchTerm(searchTerm));
		} else {
			updateSearchTerm(e.target.value);
		}
	};
	return (
		<div className="blend-search">
			<div className="aboveDroplets position-relative">
				<div className="container-md">
					<div className="row justify-content-center align-items-center my-3">
						<div className="col-12">
							<span
								className="float-start close"
								onClick={() => {
									dispatch(setIsSearchFilterOpen(false));
								}}
							>
								&#10005;
							</span>
							<h3 className="text-center">Find your perfect blend</h3>
						</div>
					</div>

					<div className="row justify-content-center">
						<div className="col-12 col-md-7">
							<div className="input-group">
								<span className="input-group-text border-0" id="search-addon">
									<i className="fas fa-search"></i>
								</span>
								<input
									autoFocus
									type="search"
									className="form-control"
									id="search-input"
									placeholder="Search"
									aria-label="Search"
									aria-describedby="search-addon"
									onChange={handleKeyPress}
									onKeyPress={handleKeyPress}
									value={searchTerm}
								/>
							</div>
						</div>
					</div>
				</div>
				<footer className="sticky-footer">
					<div className="row justify-content-center align-items-center h-100">
						<div className="col-6 text-end">
							<button className="text-uppercase clear">Clear All</button>
						</div>
						<div className="col-6 text-start">
							<button className="text-uppercase apply">Search</button>
						</div>
					</div>
				</footer>
			</div>
		</div>
	);
};

export default Search;
