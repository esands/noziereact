import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const initialState = {
	isModalOpen: false,
	modalType: "startOver",
};

export const appSlice = createSlice({
	name: "app",
	initialState,
	// The `reducers` field lets us define reducers and generate associated actions
	reducers: {
		setIsModalOpen: (state, { payload }) => {
			state.isModalOpen = payload;
		},
		setModalType: (state, { payload }) => {
			state.modalType = payload;
		},
	},
});

export const { setIsModalOpen, setModalType } = appSlice.actions;

export default appSlice.reducer;
