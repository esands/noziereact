import { request } from "../../app/axios";

export const fetchBlends = async () => {
	let data = await request("blends");

	return data;
};
