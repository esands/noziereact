import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { RootState } from "app/store";
import { DefaultRootState } from "react-redux";

type BuiltBlendState = {
	selectedBlends: Array<any>;
	sliderValue: number[];
	lockedSliders: boolean[];
	total: number;
	isStandardLayout: boolean;
	error: string;
	lastAdjusted: number;
	nextBlendsValueToAdd: number;
	topMiddleBase: any;
	colorPalette: any;
	showDelete: boolean;
};

const initialState: BuiltBlendState = {
	selectedBlends: [],
	sliderValue: [100],
	lockedSliders: [true],
	total: 100,
	isStandardLayout: false,
	error: "",
	lastAdjusted: 1,
	nextBlendsValueToAdd: 100,
	topMiddleBase: null,
	colorPalette: null,
	showDelete: false,
};

export const builtBlendSlice = createSlice({
	name: "builtBlend",
	initialState,
	// The `reducers` field lets us define reducers and generate associated actions
	reducers: {
		setSelectedBlends: (state, { payload }) => {
			state.selectedBlends = state.selectedBlends.map((blend, i) => ({
				...blend,
				value: payload[i],
			}));
			//state.selectedBlends = payload;
		},
		setNextBlendValueToAdd: (state, { payload }) => {
			state.nextBlendsValueToAdd = payload;
			//state.selectedBlends = payload;
		},
		addSelectedBlend: (state, { payload }) => {
			state.selectedBlends = [...state.selectedBlends, payload];
		},
		removeSelectedBlend: (state, { payload }) => {
			state.selectedBlends = state.selectedBlends.filter(
				(blend, i) => i !== payload
			);
		},
		setSliderValueAction: (state, { payload }) => {
			state.sliderValue = payload;
		},
		setLockedSlidersAction: (state, { payload }) => {
			state.lockedSliders = payload;
		},
		setTotalAction: (state, { payload }) => {
			state.total = payload;
		},
		setErrorAction: (state, { payload }) => {
			state.error = payload;
		},
		setLastAdjustedAction: (state, { payload }) => {
			state.lastAdjusted = payload;
		},
		setBlendData: (state, { payload }) => {
			state.topMiddleBase = payload[0];
			state.colorPalette = payload[1];
		},
		resetBlend: (state) => {
			state.selectedBlends = [];
			state.nextBlendsValueToAdd = 100;
		},
		setShowDeleteAction: (state, { payload }) => {
			state.showDelete = payload;
		},
		setSelectedBlendsValues: (
			state,
			{
				payload: { totalToDistribute, totalToAddToFoundation, foundationBlend },
			}
		) => {
			state.selectedBlends = [
				...state.selectedBlends.map((blend, i) => {
					if (i === 0)
						return {
							...blend,
							value: foundationBlend + totalToAddToFoundation,
						};

					return { ...blend, value: totalToDistribute };
				}),
			];
		},
	},
});

export const {
	addSelectedBlend,
	removeSelectedBlend,
	setSliderValueAction,
	setLockedSlidersAction,
	setTotalAction,
	setErrorAction,
	setLastAdjustedAction,
	setSelectedBlends,
	setNextBlendValueToAdd,
	setBlendData,
	setSelectedBlendsValues,
	resetBlend,
	setShowDeleteAction,
} = builtBlendSlice.actions;

export const selectSelectedBlends = (state: RootState) =>
	state.builtBlend.selectedBlends;
export const selectSliderValue = (state: RootState) =>
	state.builtBlend.sliderValue;
export const selectLockedSliders = (state: RootState) =>
	state.builtBlend.lockedSliders;
export const selectTotal = (state: RootState) => state.builtBlend.total;
export const selectError = (state: RootState) => state.builtBlend.error;
export const selectLastAdjusted = (state: RootState) =>
	state.builtBlend.lastAdjusted;
export const selectNextValueToAdd = (state: RootState) =>
	state.builtBlend.nextBlendsValueToAdd;

export const selectBlendIngredients = (state: RootState) => {
	// TODO : Loop over all ingredients to get top middle base values
	const blendCategories = state.builtBlend.selectedBlends.map((blend: any) =>
		Object.entries(blend.fragrances).map(([key, value]: [string, any]) => ({
			name: value.name,
			subCats: Object.entries(value.children),
		}))
	);

	// 	return flatten(uniqBy(blendCategories, "name"));
};

export default builtBlendSlice.reducer;
