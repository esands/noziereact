export const selectSelectedAroma = (state) => state.aroma.selectedAroma;

const selectors = {
	aroma: selectSelectedAroma,
};

export default selectors;
