import { request } from "../../app/axios";
import randomWords from "random-words";
import OrangeImage1 from "@Img/orange_1.png";
import OrangeImage2 from "@Img/orange_2-1.png";
import OrangeImage3 from "@Img/orange_cutout_3.png";
import LemonImage1 from "@Img/lemon_2.png";
import Bergamot2 from "@Img/bergamot_2.png";
import Bergamot1 from "@Img/bergamot_cutout_1.png";
import Bergamot3 from "@Img/bergamot_3.png";
import Vanilla from "@Img/vanilla_cutout.png";
import Lav from "@Img/lavendar_cutout.png";
import Lemon1 from "@Img/lemon_cutout_1.png";
import Lemon2 from "@Img/lemon_cutout_2.png";
import Neroli from "@Img/neroli_cutout.png";
import Jasmine from "@Img/jasmine_flower.png";
import Amber1 from "@Img/amber_cutout_1.png";
import Amber2 from "@Img/amber_cutout_2.png";
import Amber3 from "@Img/amber_cutout_3.png";
import Amber4 from "@Img/amber_cutout_4.png";
import Amber5 from "@Img/amber_cutout_5.png";
import Apple from "@Img/apple_cutout.png";
import FloralFruity from "@Img/floral_fruity.png";
import FloralLight from "@Img/floral_light.png";
import FloralNarcotic from "@Img/floral_narcotic.png";
import FloralPowdery from "@Img/floral_powdery.png";
import FloralRosy from "@Img/floral_rosy.png";
import FloralSweet from "@Img/floral_sweet.png";


const images = [
	FloralFruity,
	FloralLight,
	FloralNarcotic,
	FloralPowdery,
	FloralRosy,
	FloralSweet,
];

const oldImages = [
	OrangeImage1,
	OrangeImage2,
	LemonImage1,
	Bergamot1,
	Bergamot3,
	Vanilla,
	Lemon1,
	Bergamot2,
	Apple,
	Amber5,
	Amber4,
	Amber3,
	Amber2,
	Amber1,
	Jasmine,
	Neroli,
	Lav,
	Lemon2,
	OrangeImage3,
];
const moods = [
	"Playful",
	"Warm",
	"LaidBack",
	"Sexy",
	"Badass",
	"Sophisticated",
	"Soft",
	"Romantic",
	"Quirky",
	"Conventional",
	"Fun",
	"Attention Seeking",
	"Edgy",
	"Crisp",
	"Sweet",
	"Confident",
	"Understated",
	"Sensuous",
	"Calm",
	"Dark",
	"Moody",
	"Vibrant",
	"Colourful",
	"Bold",
	"Comforting",
	"Warm",
	"Cool",
	"Fresh",
	"Cool",
	"Uplifting",
	"Bubbly",
	"Deep",
	"Everyday",
	"Evening",
	"Special Occasions",
	"Versatile",
	"Spring",
	"Summer",
	"Autumn",
	"Winter",
	"All Seasons",
	"Fruity",
	"Floral",
	"Spicy",
	"Resinous",
	"Woody",
	"Fresh",
	"Green",
	"Earthy",
	"Animalic",
	"Provocative",
	"Gourmand",
];

const categories = [
	"Floral",
	"light",
	"powdery",
	"fruity",
	"rosy",
	"sweet",
	"narcotic",
	"Spicy",
	"sharp",
	"heavy",
	"fresh",
	"Resinous",
	"amber",
	"Woody",
	"rich",
	"soft",
	"Citrus",
	"Fresh",
	"aldehydes",
	"aquatic",
	"menthol",
	"Green",
	"aromatic",
	"bitterDarkSharp",
	"Agrestic / Wild / Rural",
	"green",
	"earthy",
	"warm",
	"Animalic",
	"musky",
	"leather",
	"Abstract",
	"divisive",
	"nostalgicBranded",
	"Gourmand",
	"milky",
	"alcoholNarcotic",
	"Fruity",
	"tropical",
	"juicy",
];

export const fetchBlends = async () => {
	//let data = await request("blends");
	const blends = Array(30)
		.fill({})
		.map((item, i) => {
			// Shuffle array
			const shuffled = moods.sort(() => 0.5 - Math.random());

			// Get sub-array of first n elements after shuffled
			let selected = shuffled.slice(0, 5);

			// Shuffle array
			const shuffledCat = categories.sort(() => 0.5 - Math.random());

			// Get sub-array of first n elements after shuffled
			let selectedCat = shuffledCat.slice(0, 5);
			return {
				scentName: randomWords({
					exactly: 1,
					wordsPerString: 2,
					formatter: (word) => word.toUpperCase(),
				})[0],
				description: randomWords({
					exactly: 15,
					join: " ",
				}),
				longDescription: randomWords({ exactly: 50, join: " " }),
				pricePerUnit: Math.random(),
				moods: selected,
				categories: selectedCat,
				id: i,
				fragrances: {
					0: {
						name: "Floral",
						color: "#fabbda",
						children: {
							light: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: FloralLight,
							},
							powdery: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: FloralPowdery,
							},
							fruity: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: FloralFruity,
							},
							rosy: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: FloralRosy,
							},
							sweet: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: FloralSweet,
							},
							narcotic: {
								top: 25,
								middle: 25,
								base: 0,
								total: 50,
								image: FloralNarcotic,
							},
						},
					},
					1: {
						name: "Spicy",
						color: "#ee817c",
						children: {
							sweet: {
								top: 3,
								middle: 0,
								base: 0,
								total: 3,
							},
							sharp: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							heavy: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							fresh: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
						},
					},
					2: {
						name: "Resinous",
						color: "#eb3e7d",
						children: {
							sweet: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							amber: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							light: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
						},
					},
					3: {
						name: "Woody",
						color: "#fdc58c",
						children: {
							rich: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							soft: {
								top: 0,
								middle: 0,
								base: 1,
								total: 1,
								image: images[Math.floor(Math.random() * images.length)],
							},
						},
					},
					4: {
						name: "Citrus",
						color: "#fbe68b",
						children: {
							sweet: {
								top: 5,
								middle: 0,
								base: 0,
								total: 5,
								image: images[Math.floor(Math.random() * images.length)],
							},
							fresh: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
						},
					},
					5: {
						name: "Fresh",
						color: "#74bfb8",
						children: {
							aldehydes: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							aquatic: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							menthol: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
						},
					},
					6: {
						name: "Green",
						color: "#23b574",
						children: {
							aromatic: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							bitterDarkSharp: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
						},
					},
					7: {
						name: "Agrestic / Wild / Rural",
						color: "#b0ebb5",
						children: {
							green: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							earthy: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							warm: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
						},
					},
					8: {
						name: "Animalic",
						color: "#9cd9eb",
						children: {
							musky: {
								top: 0,
								middle: 0,
								base: 16,
								total: 16,
								image: images[Math.floor(Math.random() * images.length)],
							},
							rich: {
								top: 0,
								middle: 0,
								base: 0,
								image: images[Math.floor(Math.random() * images.length)],
							},
							leather: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
						},
					},
					9: {
						name: "Abstract",
						color: "#5481c2",
						children: {
							divisive: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							nostalgicBranded: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
						},
					},
					10: {
						name: "Gourmand",
						color: "#bb93c5",
						children: {
							sweet: {
								top: 0,
								middle: 0,
								base: 25,
								total: 25,
								image: images[Math.floor(Math.random() * images.length)],
							},
							rich: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							milky: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							alcoholNarcotic: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
						},
					},
					11: {
						name: "Fruity",
						color: "#b586b2",
						children: {
							tropical: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							juicy: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
							sharp: {
								top: Math.random(),
								middle: Math.random(),
								base: Math.random(),
								total: Math.random(),
								image: images[Math.floor(Math.random() * images.length)],
							},
						},
					},
				},
			};
		});
	return blends;
};
