import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetchBlends } from "./blendsAPI";
import { uniqBy, flatten, filter } from "lodash";
import { FlatTree } from "framer-motion";

const initialState = {
	isCategoryFilterOpen: false,
	isMoodWallFilterOpen: false,
	isSearchFilterOpen: false,
	blends: [],
	moodFilter: [],
	categoryFilter: [],
	searchTerm: "",
	filterType: "all",
	isInspiredBy: false,
};

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched. Thunks are
// typically used to make async requests.
export const fetchBlendsAsync = createAsyncThunk(
	"blends/fetchBlends",
	async () => {
		const response = await fetchBlends();
		// The value we return becomes the `fulfilled` action payload
		return response;
	}
);

export const blendsSlice = createSlice({
	name: "blends",
	initialState,
	// The `reducers` field lets us define reducers and generate associated actions
	reducers: {
		setIsCategoryFilterOpen: (state, action) => {
			state.isCategoryFilterOpen = action.payload;
		},
		setIsMoodWallFilterOpen: (state, action) => {
			state.isMoodWallFilterOpen = action.payload;
		},
		setIsSearchFilterOpen: (state, action) => {
			state.isSearchFilterOpen = action.payload;
		},
		setAllFiltersClosed: (state, action) => {
			state.isSearchFilterOpen = false;
			state.isMoodWallFilterOpen = false;
			state.isCategoryFilterOpen = false;
		},
		addMoodFilter: (state, { payload }) => {
			if (!state.moodFilter.includes(payload)) {
				state.moodFilter = [...state.moodFilter, payload];
			} else {
				state.moodFilter = [
					...state.moodFilter.filter((mood) => mood != payload),
				];
			}
			if (state.moodFilter.length === 0) {
				state.filterType = "all";
			}
		},
		addCategoryFilter: (state, { payload }) => {
			if (!state.categoryFilter.includes(payload)) {
				state.categoryFilter = [...state.categoryFilter, payload];
			} else {
				state.categoryFilter = [
					...state.categoryFilter.filter((category) => category != payload),
				];
			}

			if (state.categoryFilter.length === 0) {
				state.filterType = "all";
			}
		},
		addCategoryAndChildrenFilter: (state, { payload }) => {
			const filters = [payload.name, ...payload.subCats];

			filters.forEach((filter) => {
				if (!state.categoryFilter.includes(filter)) {
					state.categoryFilter = [...state.categoryFilter, filter];
				} else {
					state.categoryFilter = [
						...state.categoryFilter.filter((category) => category != filter),
					];
				}
			});
			return state;
		},
		applyAllFilters: (state, { payload }) => {
			state.isInspiredBy = false;
			const filters =
				payload === "mood" ? state.moodFilter : state.categoryFilter;
			if (payload === "mood") {
				state.moodFilter = [...filters];
			} else {
				state.categoryFilter = [...filters];
			}
			state.isMoodWallFilterOpen = false;
			state.isCategoryFilterOpen = false;
			if (state.moodFilter.length === 0 && state.categoryFilter.length === 0) {
				state.filterType = "all";
			} else {
				state.filterType = payload;
			}
		},
		setSearchTerm: (state, { payload }) => {
			state.searchTerm = payload;
			state.filterType = "search";
			state.isSearchFilterOpen = false;
			state.isInspiredBy = false;
		},
		clearAllFilters: (state) => {
			state.moodFilter = [];
			state.categoryFilter = [];
			state.searchTerm = "";
			state.filterType = "all";
		},
		setIsInspiredBy: (state, { payload }) => {
			state.isInspiredBy = payload;
		},
	},
	// The `extraReducers` field lets the slice handle actions defined elsewhere,
	// including actions generated by createAsyncThunk or in other slices.
	extraReducers: (builder) => {
		builder
			.addCase(fetchBlendsAsync.pending, (state) => {
				state.status = "loading";
			})
			.addCase(fetchBlendsAsync.fulfilled, (state, action) => {
				state.status = "idle";
				state.blends = action.payload;
			});
	},
});

export const {
	setIsCategoryFilterOpen,
	setIsMoodWallFilterOpen,
	setIsSearchFilterOpen,
	setAllFiltersClosed,
	addMoodFilter,
	clearAllFilters,
	applyAllFilters,
	setSearchTerm,
	addCategoryFilter,
	addCategoryAndChildrenFilter,
	setIsInspiredBy,
} = blendsSlice.actions;

export const selectisCategoryFilterOpen = (state) =>
	state.blends.isCategoryFilterOpen;
export const selectisMoodWallFilterOpen = (state) =>
	state.blends.isMoodWallFilterOpen;
export const selectisSearchFilterOpen = (state) =>
	state.blends.isSearchFilterOpen;
export const selectBlends = (state) => state.blends.blends;
export const selectMoodFilters = (state) => state.blends.moodFilter;
export const selectAppliedFilters = (state) => state.blends.appliedFilters;
export const selectFilteredBySearchTerm = (state) =>
	state.blends.blends?.filter((blend) =>
		blend.scentName
			?.toLowerCase()
			.includes(state.blends.searchTerm.toLowerCase())
	);

export const selectBlendsByMoodFilter = (state) =>
	state.blends.blends?.filter((blend) =>
		blend.moods?.some((mood) => state.blends.moodFilter.includes(mood))
	);

export const selectBlendsByCategoryFilter = (state) =>
	state.blends.blends?.filter((blend) =>
		blend.categories?.some((category) =>
			state.blends.categoryFilter.includes(category)
		)
	);

export const selectBlendCategories = (state) => {
	//const allCats = [];

	const blendCategories = state.blends.blends.map((blend) =>
		Object.entries(blend.fragrances).map(([key, value]) => {
			// allCats.push(value.name);
			// allCats.push(...Object.keys(value.children));
			return {
				name: value.name,
				subCats: Object.keys(value.children),
			};
		})
	);

	return flatten(uniqBy(blendCategories, "name"));
};

export const selectIngredientsList = (id) => (state) => {
	const blendMix = {
		top: [],
		middle: [],
		base: [],
		total: [],
	};
	const blend = state.blends.blends.find((blend) => blend.id == id);
	Object.entries(blend.fragrances)?.map(([key1, value1]) => {
		Object.entries(value1.children)?.map(([key2, value2]) => {
			Object.entries(value2)?.map(([key3, value3]) => {
				if (value3 && key3 !== "image") blendMix[key3].push([key2, value3]);
			});
		});
	});
	return blendMix;
};

export default blendsSlice.reducer;
