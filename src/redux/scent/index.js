import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetchAromas } from "./blendsAPI";
import OrangeImg from "@Img/orange_1.png";

const initialState = {
	scents: {
		0: {
			scentName: "Mercural Cashmire",
			pricePerUnit: "0.50",
			fragrances: {
				0: {
					name: "Floral",
					color: "#fabbda",
					image: OrangeImg,
					children: {
						light: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						powdery: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						fruity: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						rosy: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						sweet: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						narcotic: {
							top: 25,
							middle: 25,
							base: 0,
							total: 50,
						},
					},
				},
				1: {
					name: "Spicy",
					color: "#ee817c",
					children: {
						sweet: {
							top: 3,
							middle: 0,
							base: 0,
							total: 3,
						},
						sharp: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						heavy: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						fresh: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				2: {
					name: "Resinous",
					color: "#eb3e7d",
					image: OrangeImg,
					children: {
						sweet: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						amber: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						light: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				3: {
					name: "Woody",
					color: "#fdc58c",
					image: OrangeImg,
					children: {
						rich: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						soft: {
							top: 0,
							middle: 0,
							base: 1,
							total: 1,
						},
					},
				},
				4: {
					name: "Citrus",
					color: "#fbe68b",
					image: OrangeImg,
					children: {
						sweet: {
							top: 5,
							middle: 0,
							base: 0,
							total: 5,
						},
						fresh: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				5: {
					name: "Fresh",
					color: "#74bfb8",
					image: OrangeImg,
					children: {
						aldehydes: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						aquatic: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						menthol: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				6: {
					name: "Green",
					color: "#23b574",
					image: OrangeImg,
					children: {
						aromatic: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						bitterDarkSharp: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				7: {
					name: "Agrestic / Wild / Rural",
					color: "#b0ebb5",
					image: OrangeImg,
					children: {
						green: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						earthy: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						warm: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				8: {
					name: "Animalic",
					color: "#9cd9eb",
					image: OrangeImg,
					children: {
						musky: {
							top: 0,
							middle: 0,
							base: 16,
							total: 16,
						},
						rich: {
							top: 0,
							middle: 0,
							base: 0,
						},
						leather: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				9: {
					name: "Abstract",
					color: "#5481c2",
					image: OrangeImg,
					children: {
						divisive: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						nostalgicBranded: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				10: {
					name: "Gourmand",
					color: "#bb93c5",
					image: OrangeImg,
					children: {
						sweet: {
							top: 0,
							middle: 0,
							base: 25,
							total: 25,
						},
						rich: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						milky: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						alcoholNarcotic: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				11: {
					name: "Fruity",
					color: "#b586b2",
					image: OrangeImg,
					children: {
						tropical: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						juicy: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						sharp: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
			},
		},
		1: {
			scentName: "Another Cashmire",
			pricePerUnit: "0.20",
			fragrances: {
				0: {
					name: "Floral",
					color: "#fabbda",
					image: OrangeImg,
					children: {
						light: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						powdery: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						fruity: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						rosy: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						sweet: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						narcotic: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				1: {
					name: "Spicy",
					color: "#ee817c",
					image: OrangeImg,
					children: {
						sweet: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						sharp: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						heavy: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						fresh: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				2: {
					name: "Resinous",
					color: "#eb3e7d",
					image: OrangeImg,
					children: {
						sweet: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						amber: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						light: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				3: {
					name: "Woody",
					color: "#fdc58c",
					image: OrangeImg,
					children: {
						rich: {
							top: 0,
							middle: 0,
							base: 5,
							total: 5,
						},
						soft: {
							top: 12,
							middle: 0,
							base: 0,
							total: 12,
						},
					},
				},
				4: {
					name: "Citrus",
					color: "#fbe68b",
					image: OrangeImg,
					children: {
						sweet: {
							top: 20,
							middle: 0,
							base: 0,
							total: 20,
						},
						fresh: {
							top: 30,
							middle: 0,
							base: 0,
							total: 30,
						},
					},
				},
				5: {
					name: "Fresh",
					color: "#74bfb8",
					image: OrangeImg,
					children: {
						aldehydes: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						aquatic: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						menthol: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				6: {
					name: "Green",
					color: "#23b574",
					image: OrangeImg,
					children: {
						aromatic: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						bitterDarkSharp: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				7: {
					name: "Agrestic / Wild / Rural",
					color: "#b0ebb5",
					image: OrangeImg,
					children: {
						green: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						earthy: {
							top: 0,
							middle: 0,
							base: 8,
							total: 8,
						},
						warm: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				8: {
					name: "Animalic",
					color: "#9cd9eb",
					image: OrangeImg,
					children: {
						musky: {
							top: 0,
							middle: 5,
							base: 10,
							total: 15,
						},
						rich: {
							top: 0,
							middle: 0,
							base: 0,
						},
						leather: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				9: {
					name: "Abstract",
					color: "#5481c2",
					image: OrangeImg,
					children: {
						divisive: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						nostalgicBranded: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				10: {
					name: "Gourmand",
					color: "#bb93c5",
					image: OrangeImg,
					children: {
						sweet: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						rich: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						milky: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						alcoholNarcotic: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
					},
				},
				11: {
					name: "Fruity",
					color: "#b586b2",
					image: OrangeImg,
					children: {
						tropical: {
							top: 5,
							middle: 0,
							base: 0,
							total: 5,
						},
						juicy: {
							top: 0,
							middle: 0,
							base: 0,
							total: 0,
						},
						sharp: {
							top: 5,
							middle: 0,
							base: 0,
							total: 5,
						},
					},
				},
			},
		},
	},
};

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched. Thunks are
// typically used to make async requests.
export const fetchAromasAsync = createAsyncThunk(
	"aroma/fetchAromas",
	async () => {
		const response = await fetchAromas();
		// The value we return becomes the `fulfilled` action payload
		return response;
	}
);

export const scentSlice = createSlice({
	name: "scent",
	initialState,
	// The `reducers` field lets us define reducers and generate associated actions
	reducers: {
		setIsCategoryFilterOpen: (state, action) => {
			state.isCategoryFilterOpen = action.payload;
		},
		setIsMoodWallFilterOpen: (state, action) => {
			state.isMoodWallFilterOpen = action.payload;
		},
		setIsSearchFilterOpen: (state, action) => {
			state.isSearchFilterOpen = action.payload;
		},
		setAllFiltersClosed: (state, action) => {
			state.isSearchFilterOpen = false;
			state.isMoodWallFilterOpen = false;
			state.isCategoryFilterOpen = false;
		},
	},
	// The `extraReducers` field lets the slice handle actions defined elsewhere,
	// including actions generated by createAsyncThunk or in other slices.
	// extraReducers: (builder) => {
	// 	builder
	// 		.addCase(fetchAromasAsync.pending, (state) => {
	// 			state.status = "loading";
	// 		})
	// 		.addCase(fetchAromasAsync.fulfilled, (state, action) => {
	// 			state.status = "idle";
	// 			state.aromas = action.payload;
	// 		});
	// },
});

export const {
	setIsCategoryFilterOpen,
	setIsMoodWallFilterOpen,
	setIsSearchFilterOpen,
	setAllFiltersClosed,
} = scentSlice.actions;

export const selectScents = (state) => state.scent.scents;
export const selectisMoodWallFilterOpen = (state) =>
	state.blends.isMoodWallFilterOpen;
export const selectisSearchFilterOpen = (state) =>
	state.blends.isSearchFilterOpen;

export default scentSlice.reducer;
