import { request } from "../../app/axios";

export const fetchAromas = async () => {
	let data = await request("aromas");

	return data;
};
